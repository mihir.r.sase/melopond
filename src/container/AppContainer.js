import { NavigationContainer } from '@react-navigation/native';

import Splash from './Splash';
import Login from './Login';
import SelectPreference from './SelectPreference';
import React from 'react';
import Register from './Register';
import JoinAs from './JoinAs';
import AppRoutes from '../utils/AppRoutes';
import {createStackNavigator} from '@react-navigation/stack';
import MyDrawer from '../utils/MyDrawer';
import ForgotPassword from './ForgotPassword';
const Stack = createStackNavigator();

function AppContainer() {

  return(
      <NavigationContainer>
          <Stack.Navigator initialRouteName={AppRoutes.splash}>
              <Stack.Screen name={AppRoutes.splash} component={Splash} options={{ headerShown: false}} initialParams={{ email: '' }}/>
              <Stack.Screen name={AppRoutes.login} component={Login} options={{ headerShown: false}}/>
              <Stack.Screen name={AppRoutes.register} component={Register} options={{ headerShown: false}}/>
              <Stack.Screen name={AppRoutes.joinAs} component={JoinAs} options={{ headerShown: false}} />
              <Stack.Screen name={AppRoutes.preferences} component={SelectPreference} options={{headerTitle: 'Preferences'}}/>
              <Stack.Screen name={AppRoutes.forgotPassword} component={ForgotPassword} options={{headerTitle: 'Forgot Password'}}/>
              <Stack.Screen name={AppRoutes.explore} component={MyDrawer} options={{ headerShown: false}} />

          </Stack.Navigator>

      </NavigationContainer>
  )
}
export default AppContainer;
