import React, {Component} from 'react';
import {Image, Text, View} from 'react-native';
import {Card} from 'react-native-paper';
import Mike from '../assets/images/mike.png';
import HeadPhones from '../assets/images/headphones.png';
import AsyncStorage from '@react-native-community/async-storage';
import AppConstants from '../utils/AppConstants';
import useAxios from 'axios-hooks';
import ApiRoutes from '../utils/ApiRoutes';
import AppRoutes from '../utils/AppRoutes';
import userLoading from '../utils/useLoading';
import {CommonActions} from '@react-navigation/native';



const JoinAs =(props)=>{

    const [{data, loading, error}, execute] = useAxios({url: ApiRoutes.updatePreference, method: 'POST'}, {manual: true});

    const selectRole=(role)=>{
        execute({data: {'type': 4, 'role': role}}).then(()=>{

            AsyncStorage.getItem(AppConstants.auth).then((value) => {
                if(value!==null){
                    value=JSON.parse(value);
                    value.role=role;
                    AsyncStorage.setItem( AppConstants.auth, JSON.stringify( value ) ).then(()=>{
                        props.navigation.dispatch(
                            CommonActions.reset({
                                index: 1,
                                routes: [
                                    { name: AppRoutes.preferences,params: {role:role} }
                                ],
                            })
                        );
                    });
                }
            }).catch(e=>console.log(e));

        }).catch((e)=>{console.log(e)});
    };

    userLoading(loading);
    return (
        <View style={{flex: 1, backgroundColor: '#fff', justifyContent: 'center'}}>

            <Text style={{ textAlign:'center',fontSize: 20, fontWeight: 'bold'}}>Join As</Text>
            <View style={{flexDirection: 'row',padding:12}}>
                <Card style={{flex: 1, padding: 12, backgroundColor: '#fff',marginEnd:6,height:150,width:150}} onPress={() => selectRole('listener')}
                      elevation={8}>
                    <View style={{justifyContent:'center',flex:1,alignItems:'center'}}>

                        <Image source={HeadPhones} style={{height: 80, width: 80}}/>
                    </View>
                    <Text style={{ textAlign:'center',fontSize: 20, fontWeight: 'bold'}}>Listener</Text>
                </Card>
                <Card style={{flex: 1, padding: 12, backgroundColor: '#fff',marginStart:6,height:150,width:150}} onPress={() => selectRole('musician')}
                      elevation={8}>
                    <View style={{justifyContent:'center',flex:1,alignItems:'center'}}>

                        <Image source={Mike} style={{height: 80, width: 80}}/>
                    </View>
                    <Text style={{ textAlign:'center',fontSize: 20, fontWeight: 'bold'}}>Musician</Text>
                </Card>
            </View>

        </View>
    );
};

export default JoinAs;
