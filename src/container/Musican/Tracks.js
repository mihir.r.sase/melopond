import {FlatList, Text, View} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import useAxios from 'axios-hooks';
import ApiRoutes from '../../utils/ApiRoutes';
import {IconButton} from 'react-native-paper';
import useLoading from '../../utils/useLoading';
import AppRoutes from '../../utils/AppRoutes';


const Tracks=(props)=>{

    const [songList,setSongList]=React.useState('');

    const [{data, loading, error}, execute] = useAxios({url: ApiRoutes.musicGetAll, method: 'POST'}, {manual: true});
    useLoading(loading);
    React.useEffect(() => {
        if(loading===false && data===undefined){
            execute().then((data)=>{
                setSongList(data.data.list)
            }).catch((e)=>console.log(e));
        }
    });

    React.useLayoutEffect(() => {
        props.navigation.setOptions({
            headerLeft: () => (
                <IconButton icon="menu" color={'#000'} size={25} onPress={() =>{props.navigation.openDrawer();} }/>
            ),
            headerRight: () => (
            <IconButton icon="plus" color={'#000'} size={25} onPress={() => {
                props.navigation.navigate(AppRoutes.addTrack);
            }}/>
        ),
        });
    });
    function Song({ item }) {
        return (
            <View style={{ backgroundColor: '#fff',margin:4,paddingBottom:6,paddingTop:6,flexDirection:'row'}}>
                <View style={{flex:4,justifyContent:'center'}}>
                    <Text style={{fontSize:14,fontWeight:'bold'}}>{item.title}</Text>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <Text style={{fontSize:12,color:'#323232'}}>{item.musician_name}</Text>

                    </View>
                </View>
                <View style={{flex:1,justifyContent:'center'}}>
                    <View style={{alignItems:'center'}}>
                        <Icon name="play-circle" size={25} color="#33CCFF"/>
                    </View>
                    <Text style={{fontSize:10,textAlign:'center'}}>03:23</Text>
                </View>
            </View>
        );
    }

    return(
        <View style={{flex:1,backgroundColor:'#fff',padding:18}}>
            <FlatList
                style={{marginTop: 8}}
                data={songList}
                renderItem={({item}) => <Song item={item}/>}
                keyExtractor={item => item.slug}
            />
        </View>
    );
};
export default Tracks
