import React, {Component} from 'react';
import {FlatList, Image, Linking, TextInput, View} from 'react-native';
import Song from '../Song';
import {Button, Card, Chip, IconButton, Text,Portal,Modal} from 'react-native-paper';
import useAxios from 'axios-hooks';
import ApiRoutes from '../../utils/ApiRoutes';
import useLoading from '../../utils/useLoading';
import AppConstants from '../../utils/AppConstants';
import {useFocusEffect} from '@react-navigation/native';





const HireMusicianRequests =(props)=>{

    const [requestList,setRequestList]= React.useState();
    const [modal,setModal]= React.useState({
        visible: false,
        data: '',
    });
    const handleChange = (value, name) => {
        setModal(prevState => ({...prevState, [name]: value}));
    };

    const [{data, loading, error}, execute] = useAxios({
        url: ApiRoutes.hireRequestsListener,
        method: 'POST',
    }, {manual: true});
    useLoading(loading);

    // React.useEffect(() => {
    //     if(loading===false && data===undefined){
    //         execute().then((data)=>{
    //             setRequestList(data.data.list);
    //         }).catch(e=>console.log(e));
    //     }
    // });

    useFocusEffect(
        React.useCallback(() => {
            // Do something when the screen is focused
            if(loading===false && data===undefined){
                execute().then((data)=>{
                    setRequestList(data.data.list);
                }).catch(e=>console.log(e));
            }
            return () => {
                // Do something when the screen is unfocused
                // Useful for cleanup functions
            };
        }, []),
    );

    React.useLayoutEffect(() => {
        props.navigation.setOptions({
            headerLeft: () => (
                <IconButton icon="menu" color={'#000'} size={25} onPress={() =>{props.navigation.openDrawer();} }/>
            ),
        });
    });


    const MakePayment =(props)=>{
        const [couponCode,setCouponCode]=React.useState('');
        const [codeApplied,setCodeApplied]=React.useState(modal.data.code_apply);
        const [responseMessage,setMessage]=React.useState(codeApplied?'Coupon Code already applied':'');
        const [finalAmount,setFinalAmount]=React.useState(codeApplied?(modal.data.budget-modal.data.discount_amount):modal.data.budget);
        const [{data, loading, error}, execute] = useAxios({
            url: ApiRoutes.applyCoupon,
            method: 'POST',
        }, {manual: true});
        useLoading(loading);

        const applyCoupon=()=>{
            execute({data:{id:modal.data.id,budget:modal.data.budget,code:couponCode}}).then((data)=>{
                setMessage(data.data.message);
                setFinalAmount(modal.data.budget-data.data.amount);
                setCodeApplied(true)
            }).catch(e=>console.log(e));
        };

        const _hideModal = () => handleChange(false,'visible');

        const proceedToPayment =()=>{
            Linking.openURL(AppConstants.baseUrlForPayment+modal.data.id+"/"+modal.data.type).catch(err => console.error("Couldn't load page", err));
        };

        return(
            <Portal>
                <Modal visible={modal.visible} onDismiss={_hideModal}>
                    <View style={{backgroundColor:'#fff',padding:30,marginStart:30,marginEnd:30}}>
                        {!codeApplied &&   <View style={{flexDirection:'row',alignItems:'center'}}>
                            <TextInput onChangeText={text => setCouponCode(text)} value={couponCode}
                                       style={{fontSize:16, height: 65, backgroundColor: '#f8f8f8', paddingStart: 12,flex:4,marginEnd:4}} placeholder={'Coupon Code'}/>
                            <Button style={{height: 50, justifyContent: 'center',flex:1,marginStart:4}} mode="contained"
                                    onPress={() => applyCoupon()} color={'#5cb85c'} dark={true}>Apply</Button>

                        </View>}

                        <Text>{responseMessage}</Text>
                        <Text>Final Amount ₹{finalAmount}</Text>
                        <Button style={{height: 50, justifyContent: 'center', marginTop: 12}} mode="contained"
                                onPress={() => proceedToPayment()} color={'#33CCFF'} dark={true}>Proceed</Button>


                    </View>
                </Modal>

            </Portal>
        )
    };

    function RequestItem({item}) {
        return (

            <Card style={{backgroundColor: '#fff',margin:8,padding:12}} elevation={8}>
                <View style={{flex:1}}>
                    <View style={{flexDirection:'row',marginBottom:4}}>
                        <Text style={{flex:1,fontWeight:'bold'}}>Event</Text>
                        <Text style={{flex:1,textAlign:'right'}}>{item.event_category}</Text>
                    </View>
                    <View style={{flexDirection:'row',marginBottom:4}}>
                        <Text style={{flex:1,fontWeight:'bold'}}>Event Date</Text>
                        <Text style={{flex:1,textAlign:'right'}}>{item.event_date}</Text>
                    </View>
                    <View style={{flexDirection:'row',marginBottom:4}}>
                        <Text style={{flex:1,fontWeight:'bold'}}>Musician Name</Text>
                        <Text style={{flex:1,textAlign:'right'}}>{item.musician_name}</Text>
                    </View>
                    <View style={{flexDirection:'row',marginBottom:4}}>
                        <Text style={{flex:1,fontWeight:'bold'}}>Budget</Text>
                        <Text style={{flex:1,textAlign:'right'}}>₹{item.budget}</Text>
                    </View>
                    <View style={{flexDirection:'row',marginBottom:4}}>
                        <Text style={{flex:1,fontWeight:'bold'}}>Location</Text>
                        <Text style={{flex:1,textAlign:'right'}}>{item.location}</Text>
                    </View>
                    <View style={{flexDirection:'row',marginBottom:4}}>
                        <Text style={{flex:1,fontWeight:'bold'}}>Mobile</Text>
                        <Text style={{flex:1,textAlign:'right'}}>{item.mobile_no}</Text>
                    </View>
                    <View style={{flexDirection:'row',marginBottom:4}}>
                        <Text style={{flex:1,fontWeight:'bold'}}>Status</Text>
                        <View style={{justifyContent:'flex-end'}}>
                            <Chip textStyle={{color:'#fff'}}
                                style={item.status==='Received'?{backgroundColor:'#ffc107'}:(item.status==='Accept'?{backgroundColor:'#4caf50'}:{backgroundColor:'#ff5722'})}>{item.status}</Chip>
                        </View>
                        {/*<Text style={{flex:1,textAlign:'right'}}>{item.status}</Text>*/}
                    </View>
                    <View style={{flexDirection:'row',marginBottom:8}}>
                        <Text style={{flex:1,fontWeight:'bold'}}>Message</Text>
                        <Text style={{flex:1,textAlign:'right'}}>{item.message}</Text>
                    </View>
                    { item.status==='Accept' &&
                        <Button style={{height: 50, justifyContent: 'center', flex: 1, marginStart: 4}} mode="contained"
                                onPress={() => {handleChange(true,'visible');handleChange(item,'data')}} color={'#33CCFF'} dark={true}>Make
                            Payment</Button>
                    }
                </View>
            </Card>
        );
    }

    return(
        <View style={{backgroundColor: '#fff', padding: 10,flex:1}}>
            <MakePayment/>
            <FlatList
                style={{marginTop: 8}}
                data={requestList}
                renderItem={({item}) => <RequestItem item={item}/>}
                keyExtractor={item => item.id}
            />

        </View>
    )
};

export default HireMusicianRequests;
