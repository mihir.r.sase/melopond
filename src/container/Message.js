import {Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import React from 'react';
import {Card} from 'react-native-paper';
import HTMLView from 'react-native-htmlview/HTMLView';


function Message({ item }) {
    return (
        <Card style={{backgroundColor: '#fff',margin:8,padding:12}} elevation={8}>
        <View style={{ backgroundColor: '#fff',paddingBottom:6,paddingTop:6}}>
           <Text style={{fontSize:12,fontWeight:'bold'}}>{item.date}</Text>
            {/*<Text style={{fontSize:16,marginTop:4}}>{item.message.data}</Text>*/}
            <HTMLView
                value={item.message.data}
            />
        </View>
        </Card>
    );
}
export default Message;
