
import React, {Component} from "react";
import {View, Text, Image, TextInput} from 'react-native';
import {Button} from 'react-native-paper';
import useAxios from 'axios-hooks';
import ApiRoutes from '../utils/ApiRoutes';
import useLoading from '../utils/useLoading';
const ForgotPassword = (props) => {

    const [state, setState] = React.useState({
        email: '',
    });
    const handleChange = (value, name) => {
        setState(prevState => ({...prevState, [name]: value}));
    };

    const [{data, loading, error}, execute] = useAxios({url: ApiRoutes.forgot_password, method: 'POST'}, {manual: true});
    useLoading(loading);
    const sendEmail=()=>{
        execute({data: {'email': state.email}}).then(()=>{
            console.log("Email Sent");
        }).catch(e=>console.log(e));
    };

    return(
        <View style={{flex:1,backgroundColor:'#fff',padding:18}}>
            <Text style={{textAlign:'center'}}>Enter your email below and we will send you instructions on how to change your password.</Text>
            <TextInput style={{marginTop: 12,  backgroundColor: '#f8f8f8', paddingStart: 12 ,fontSize:16, height: 65}} placeholder={'Email'}
                       onChangeText={text => handleChange(text, 'email')} value={state.email}/>
            <Button style={{height: 50, justifyContent: 'center', marginTop: 22}} mode="contained"
                    onPress={() =>sendEmail() } color={'#33CCFF'} dark={true}>Send</Button>
        </View>
    )
};
export default ForgotPassword;
