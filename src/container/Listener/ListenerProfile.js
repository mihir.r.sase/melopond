import React, {Component} from 'react';
import {Image, Text, View, ScrollView, FlatList, TextInput, TouchableOpacity} from 'react-native';
import Rahman from '../../assets/images/rahman.png';
import {Button, IconButton} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';
import Song from '../Song';
import Arijit from '../../assets/images/arijit.png';
import Sonu from '../../assets/images/sonu.png';
import useAxios from 'axios-hooks';
import ApiRoutes from '../../utils/ApiRoutes';
import useLoading from '../../utils/useLoading';
import AppRoutes from '../../utils/AppRoutes';
import {useFocusEffect} from '@react-navigation/native';


const ListenerProfile = (props) => {
    let TRACK_DATA = [
        {
            id: '1',
            name: 'Tera Jaisa Yaar Kaha',
            album: 'Yaarana',
            genre: 'Traditional',
            length: '03:06',
        },
        {
            id: '2',
            name: 'Neele Neele Ambar par',
            album: 'Kalaakaar',
            genre: 'Traditional',
            length: '03:20',
        },
        {
            id: '3',
            name: 'Tujhse Naaraz Nahi Zindagi',
            album: 'Masoom',
            genre: 'Romantic',
            length: '04:02',
        },
        {
            id: '4',
            name: 'Tera Jaisa Yaar Kaha',
            album: 'Yaarana',
            genre: 'Traditional',
            length: '03:06',
        },
        {
            id: '5',
            name: 'Neele Neele Ambar par',
            album: 'Kalaakaar',
            genre: 'Traditional',
            length: '03:20',
        },
        {
            id: '6',
            name: 'Tujhse Naaraz Nahi Zindagi',
            album: 'Masoom',
            genre: 'Romantic',
            length: '04:02',
        },

    ];

    const ARTIST_DATA = [
        {
            id: '1',
            title: 'Arijit Singh',
            background: Arijit,
            charges: '₹8000',
        },
        {
            id: '2',
            title: 'Sonu Nigam',
            background: Sonu,
            charges: '₹10000',
        },
        {
            id: '3',
            title: 'A.R. Rahman',
            background: Rahman,
            charges: '₹4500',
        },
    ];

    const [profileData, setProfileData] = React.useState('');

    const [{data, loading, error}, execute] = useAxios({
        url: ApiRoutes.listenerProfile,
        method: 'POST',
    }, {manual: true});
    useLoading(loading);

    React.useLayoutEffect(() => {
        props.navigation.setOptions({
            headerLeft: () => (
                <IconButton icon="menu" color={'#000'} size={25} onPress={() => {
                    props.navigation.openDrawer();
                }}/>
            ), headerRight: () => (
                <IconButton icon="pencil" color={'#000'} size={25} onPress={() => {
                    props.navigation.navigate(AppRoutes.listenerEditProfile, profileData.profile);
                }}/>
            ),
        });
    });

    useFocusEffect(
        React.useCallback(() => {
            // Do something when the screen is focused
            if (loading === false && data === undefined) {
                execute().then((data) => {
                    setProfileData(data.data);
                }).catch(e => console.log(e));
            }
            return () => {
                // Do something when the screen is unfocused
                // Useful for cleanup functions
            };
        }, []),
    );




    function ArtistItem({item}) {
        return (
            <TouchableOpacity onPress={() => {props.navigation.navigate(AppRoutes.musicianProfile,{slug:item.slug})}} style={{flexDirection: 'column', alignItems: 'center',flex:1}}>
            <View style={{backgroundColor: '#fff', height: 130, width: 130, margin: 4, flex: 1, flexDirection: 'column', alignItems: 'center',}}>
                <Image source={{uri: item.profile_pic}}
                       style={{height: 130, width: 130, position: 'absolute', borderRadius: 130 / 2}}/>

                <View style={{flex: 1, justifyContent: 'flex-end'}}>
                    <Text style={{
                        fontSize: 14,
                        textAlign: 'center',
                        color: '#fff',
                        fontWeight: 'bold',
                        marginBottom: 12,
                    }}>{item.name}</Text>
                </View>

            </View>
            </TouchableOpacity>
        );
    }
    let likedSongs=profileData?.like_song?.slice();
    let followings=profileData?.follow?.slice();
    let favourites=profileData?.favourite?.slice();
    return (
        <ScrollView>
            <View style={{backgroundColor: '#fff', padding: 18}}>

                <View style={{flexDirection: 'row', flex: 1}}>
                    <View style={{justifyContent: 'center'}}>
                        <Image source={{uri: profileData.profile?.profile_pic}}
                               style={{height: 100, width: 100, borderRadius: 100 / 2}}/>
                    </View>
                    <View style={{flex: 1, marginStart: 28}}>
                        <Text style={{fontSize: 20, fontWeight: 'bold'}}>{profileData.profile?.name}</Text>
                        <Text style={{fontSize: 14, marginTop: 8}}>{profileData.profile?.about_me}</Text>
                    </View>
                </View>

                <View style={{flexDirection: 'row', padding: 8, marginTop: 28, backgroundColor: '#f8f8f8'}}>
                    <View style={{flex: 1, padding: 8}}>
                        <Text style={{
                            textAlign: 'center',
                            color: '#33CCFF',
                            fontSize: 18,
                            fontWeight: 'bold',
                        }}>{profileData.profile?.like}</Text>
                        <Text style={{textAlign: 'center', fontSize: 14, marginTop: 4}}>Liked Songs</Text>
                    </View>
                    <View style={{
                        flex: 1,
                        padding: 8,
                        borderLeftColor: '#323232',
                        borderLeftWidth: 1,
                    }}>
                        <Text style={{
                            textAlign: 'center',
                            color: '#33CCFF',
                            fontSize: 18,
                            fontWeight: 'bold',
                        }}>{profileData.profile?.follow}</Text>
                        <Text style={{textAlign: 'center', fontSize: 14, marginTop: 4}}>Following</Text>
                    </View>
                </View>


                <View style={{marginTop: 28}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Text style={{fontSize: 18, color: '#33CCFF', fontWeight: 'bold', flex: 1}}>Liked Songs</Text>
                        <Text style={{fontSize: 14, textAlign: 'right', flex: 1}}
                              onPress={() => {props.navigation.navigate(AppRoutes.songList, {songs: profileData.like_song, title: 'Liked Songs',});}}
                        >View All</Text>
                    </View>
                    <FlatList
                        style={{marginTop: 8}}
                        data={likedSongs?.splice(0, 3)}
                        renderItem={({item}) => <Song item={item}/>}
                        keyExtractor={item => item.slug}
                    />
                </View>

                <View style={{marginTop: 28}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Text style={{fontSize: 18, color: '#33CCFF', fontWeight: 'bold', flex: 1}}>Followings</Text>
                        <Text style={{fontSize: 14, textAlign: 'right', flex: 1}}>View All</Text>
                    </View>
                    <FlatList
                        style={{marginTop: 8, padding: 4}}
                        data={followings?.splice(0, 3)}
                        renderItem={({item}) => <ArtistItem item={item}/>}
                        keyExtractor={item => item.id}
                        numColumns={2}
                    />
                </View>

                <View style={{marginTop: 28}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Text style={{fontSize: 18, color: '#33CCFF', fontWeight: 'bold', flex: 1}}>Favourites</Text>
                        <Text style={{fontSize: 14, textAlign: 'right', flex: 1}}  onPress={() => {props.navigation.navigate(AppRoutes.songList, {songs: profileData.favourite, title: 'Favourite Songs',});}}
                        >View All</Text>
                    </View>
                    <FlatList
                        style={{marginTop: 8}}
                        data={favourites?.splice(0, 3)}
                        renderItem={({item}) => <Song item={item}/>}
                        keyExtractor={item => item.slug}
                    />
                </View>
            </View>
        </ScrollView>
    );
};
export default ListenerProfile;
