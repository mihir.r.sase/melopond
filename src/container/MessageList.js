import React, {Component} from 'react';
import {FlatList, Image, View} from 'react-native';
import Song from './Song';
import {Button, Card, Chip, IconButton, Text} from 'react-native-paper';
import useAxios from 'axios-hooks';
import ApiRoutes from '../utils/ApiRoutes';
import useLoading from '../utils/useLoading';
import Message from './Message';


const MessageList =(props)=>{

    const [messageList,setMessageList]= React.useState();

    const [{data, loading, error}, execute] = useAxios({
        url: ApiRoutes.getMessages,
        method: 'POST',
    }, {manual: true});
    useLoading(loading);

    React.useEffect(() => {
        if(loading===false && data===undefined){
            execute().then((data)=>{
                setMessageList(data.data.list);
            }).catch(e=>console.log(e));
        }
    });

    React.useLayoutEffect(() => {
        props.navigation.setOptions({
            headerLeft: () => (
                <IconButton icon="menu" color={'#000'} size={25} onPress={() =>{props.navigation.openDrawer();} }/>
            ),
        });
    });




    return(
        <View style={{backgroundColor: '#fff', padding: 10,flex:1}}>
            <FlatList
                style={{marginTop: 8}}
                data={messageList}
                renderItem={({item}) => <Message item={item}/>}
                keyExtractor={item => item.id}
            />

        </View>
    )
};

export default MessageList;
