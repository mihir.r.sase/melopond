import React, {Component} from 'react';
import {FlatList, Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {Card, IconButton} from 'react-native-paper';
import Evening from '../assets/images/evening.png';
import Morning from '../assets/images/morning.png';
import Night from '../assets/images/night.png';
import Arijit from '../assets/images/arijit.png';
import Sonu from '../assets/images/sonu.png';
import Rahman from '../assets/images/rahman.png';
import Mike from '../assets/images/mike.png';
import HeadPhones from '../assets/images/headphones.png';
import Music from '../assets/images/music.png';
import Sound from '../assets/images/sound.png';
import {CommonActions, useFocusEffect} from '@react-navigation/native';
import MyDrawer from '../utils/MyDrawer';
import useAxios from 'axios-hooks';
import ApiRoutes from '../utils/ApiRoutes';
import useLoading from '../utils/useLoading';
import AppRoutes from '../utils/AppRoutes';
import Song from './Song';


const RADIO_DATA = [
    {
        id: '1',
        title: 'Midnight Melody',
        background: Night,
    },
    {
        id: '2',
        title: 'Morning Music',
        background: Morning,
    },
    {
        id: '3',
        title: 'Evening Jazz',
        background: Evening,
    },
];
const ARTIST_DATA = [
    {
        id: '1',
        title: 'Arijit Singh',
        background: Arijit,
    },
    {
        id: '2',
        title: 'Sonu Nigam',
        background: Sonu,
    },
    {
        id: '3',
        title: 'A.R. Rahman',
        background: Rahman,
    },
];
const GENRE_DATA = [
    {
        id: '1',
        title: 'Indian Classical Music',
        icon:Mike
    },
    {
        id: '2',
        title: 'Indian Jazz',
        icon:HeadPhones
    },
    {
        id: '3',
        title: 'Indian Pop',
        icon:Music
    },
    {
        id: '4',
        title: 'Indian Rock',
        icon:Sound
    },
    {
        id: '5',
        title: 'Raga Rock',
        icon:Mike
    },
    {
        id: '6',
        title: 'Trance',
        icon:HeadPhones
    },
    {
        id: '7',
        title: 'Blues',
        icon:Music
    },
    {
        id: '8',
        title: 'EDM',
        icon:Sound
    },
    {
        id: '9',
        title: 'Classical',
        icon:Mike
    },
    {
        id: '10',
        title: 'Country',
        icon:HeadPhones
    },
    {
        id: '11',
        title: 'Electronic',
        icon:Music
    },
    {
        id: '12',
        title: 'Folk',
        icon:Sound
    },
    {
        id: '13',
        title: 'Jazz',
        icon:Mike
    },
    {
        id: '14',
        title: 'New Age',
        icon:HeadPhones
    },
    {
        id: '15',
        title: 'Reggae',
        icon:Music
    },
    {
        id: '16',
        title: 'Rock',
        icon:Sound
    },
    {
        id: '17',
        title: 'Metal',
        icon:Mike
    },
];



const Explore =(props)=>{

    const [{data, loading, error}, execute] = useAxios({url: ApiRoutes.getHomeData, method: 'POST'}, {manual: true});
    useLoading(loading);

    const [homeData,setHomeData] = React.useState('');

    React.useLayoutEffect(() => {
        props.navigation.setOptions({
            headerLeft: () => (
                <IconButton icon="menu" color={'#000'} size={25} onPress={() =>{props.navigation.openDrawer();} }/>
            ),
        });
    });

    useFocusEffect(
        React.useCallback(() => {
            // Do something when the screen is focused
            if (loading === false && data === undefined) {
                execute().then((data) => {
                    setHomeData(data.data);
                }).catch(e => console.log(e));
            }
            return () => {
                // Do something when the screen is unfocused
                // Useful for cleanup functions
            };
        }, []),
    );

    function RadioItem({item}) {
        return (

            <Card style={{backgroundColor: '#fff', height: 150, width: 150, margin:8}}
                  elevation={8}>
                <Image source={{uri: item.image}} style={{height: 150, width: 150, position: 'absolute'}}/>
                <View style={{flex: 1, justifyContent: 'center'}}>
                    <Text style={{fontSize: 20, textAlign: 'center', color: '#fff',fontWeight:'bold'}}>{item.title}</Text>
                </View>
            </Card>
        );
    }

    function ArtistItem({item}) {
        return (
            <TouchableOpacity onPress={() => {props.navigation.navigate(AppRoutes.musicianProfile,{slug:item.slug})}} style={{flexDirection: 'column', alignItems: 'center',flex:1}}>
            <Card style={{backgroundColor: '#fff', height: 150, width: 150, margin:8}}
                  elevation={8}>
                <Image source={{uri: item.profile_pic}} style={{height: 150, width: 150, position: 'absolute'}}/>
                <View style={{flex: 1, justifyContent: 'flex-end'}}>
                    <Text style={{fontSize: 14, textAlign: 'center', color: '#fff',marginBottom:12,fontWeight:'bold'}}>{item.name}</Text>
                </View>
            </Card>
            </TouchableOpacity>
        );
    }

    function GenreItem({item}) {
        return (

            <Card style={{backgroundColor: '#fff', height: 150, width: 150, margin:8}}
                  elevation={8}>
                <View style={{flex:1,justifyContent:'center'}}>
                    <View style={{alignItems:'center'}}>
                        <Image source={item.icon} style={{height: 40, width: 40}}/>
                    </View>
                    <Text style={{textAlign:'center',marginTop:8}}>{item.title}</Text>
                </View>
            </Card>
        );
    }

    let music=homeData?.home?.slice();
    return (
        <ScrollView>

            <View style={{backgroundColor: '#fff', paddingTop: 18,paddingBottom:18}}>
                <Text style={{fontSize: 18, color: '#323232', fontWeight: 'bold',marginStart:18}}>Radio</Text>
                <FlatList
                    style={{padding: 4}}
                    data={homeData?.radio}
                    renderItem={({item}) => <RadioItem item={item}/>}
                    keyExtractor={item => item.slug}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                />
                <Text style={{fontSize: 18, color: '#323232', fontWeight: 'bold', marginTop: 12,marginStart:18}}>Artists</Text>
                <FlatList
                    style={{padding: 4}}
                    data={homeData?.artist}
                    renderItem={({item}) => <ArtistItem item={item}/>}
                    keyExtractor={item => item.id.toString()}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                />

                <View style={{marginTop: 12,marginStart:18,marginEnd:18}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Text style={{fontSize: 18, color: '#323232', fontWeight: 'bold',flex:1}}>Music</Text>
                        <Text style={{fontSize: 14, textAlign: 'right', flex: 1}}
                              onPress={() => {props.navigation.navigate(AppRoutes.songList, {songs: homeData.home, title: 'Music',});}}
                        >View All</Text>
                    </View>
                    <FlatList
                        style={{marginTop: 8}}
                        data={music?.splice(0, 3)}
                        renderItem={({item}) => <Song item={item} navigation={props.navigation}/>}
                        keyExtractor={item => item.slug}
                    />
                </View>
            </View>
        </ScrollView>
    );
};

export default Explore;
