/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import AppContainer from './src/container/AppContainer';
import WebService from './src/utils/Webservice';
import {Provider} from 'react-redux';
import store from './src/utils/store';
import Loading from './src/utils/Loading';
import MyDrawer from './src/utils/MyDrawer';
import {Provider as PaperProvider} from 'react-native-paper';

const App: () => React$Node = () => {

    React.useEffect(() => {
        WebService.init();
    });
    return (
        <Provider store={store}>
            <PaperProvider>
                <AppContainer/>
                <Loading/>
            </PaperProvider>
        </Provider>


    );
};
export default App;
