import produce from 'immer';
import {APP_CONSTANTS} from './appAction';

const initState = {
    loading: false,
};

const appReducer = (state = initState, {type, payload}) => {
    return produce(state, draft => {

        if (type === APP_CONSTANTS.LOADING) {
            draft.loading = payload;
        }
    });
};

export default appReducer;
