// import React, {Component} from 'react';
// import {Image, Text, View} from 'react-native';
// import {Card} from 'react-native-paper';
// import Album from '../assets/images/album.jpg';
// import Slider from 'react-native-slider/lib/Slider';
// import Icon from 'react-native-vector-icons/FontAwesome';
// import TrackPlayer from 'react-native-track-player';
// import {useTrackPlayerProgress} from 'react-native-track-player/lib/hooks';
//
// // STATE READY :  2
// // STATE NONE :  0
// // STATE PLAYING :  3
// // STATE PAUSED :  2
// // STATE STOP :  1
// // STATE CONNECTING :  8
// // STATE BUFFERING :  6
//
// const Player=(props)=>{
//
//     const song = props.route.params.item;
//     const [playerState,setPlayerState]= React.useState();
//     const { position, bufferedPosition, duration } = useTrackPlayerProgress();
//
//     React.useEffect(() => {
//         console.log("USE EFFECT");
//         let track = {
//             id: song.slug, // Must be a string, required
//             url: song.url, // Load media from the network
//             title: song.title,
//             artist: song.name,
//             artwork: song.image, // Load artwork from the network
//         };
//
//
//         TrackPlayer.add([track]).then(()=>{
//             TrackPlayer.play().then(()=>{
//
//             });
//
//         });
//
//         TrackPlayer.addEventListener('playback-state', async (state) => {
//             console.log("State Change : "+JSON.stringify(state));
//             setPlayerState(state.state);
//         });
//
//         return () => {
//             TrackPlayer.stop();
//         };
//     },[]);
//
//
//
//     const togglePlayPause =()=>{
//         TrackPlayer.getState().then((state)=>{
//             if(state=== TrackPlayer.STATE_PLAYING){
//                 TrackPlayer.pause();
//             }else if(state=== TrackPlayer.STATE_PAUSED){
//                 TrackPlayer.play();
//             }
//         });
//     };
//
//
//     const calculateDuration=(totalSeconds)=>{
//         let m = Math.floor(totalSeconds % 3600 / 60);
//         let s = Math.floor(totalSeconds % 3600 % 60);
//         return (m > 0 && m < 10 ? '0' + m : m) + ':' + (s < 10 ? '0' + s : s)
//     };
//
//
//
//
//
//
//
//     return (
//         <View style={{backgroundColor: '#000', flex: 1}}>
//             <View style={{flex: 2, padding: 38, justifyContent: 'center'}}>
//                 <Card style={{backgroundColor: '#fff'}}
//                       elevation={8}>
//                     <Image source={{uri: song.image}} style={{height: undefined, width: '100%', aspectRatio: 1}}/>
//                 </Card>
//             </View>
//             <View style={{flex: 1.6, marginStart: 38, marginEnd: 38}}>
//                 <Text style={{color: '#f4f3f4', textAlign: 'center', fontWeight: 'bold', fontSize: 22}}>{song.title}</Text>
//                 <Text style={{color: '#f4f3f4', textAlign: 'center', fontSize: 12}}>{song.name}</Text>
//                 <View style={{marginTop: 18, flexDirection: 'row'}}>
//                     <Text style={{color: '#f4f3f4', fontSize: 12}}>{calculateDuration(position)}</Text>
//                     <View style={{flex: 1}}>
//                         <Text style={{color: '#f4f3f4', fontSize: 12, textAlign: 'right'}}>{calculateDuration(song.duration)}</Text>
//                     </View>
//                 </View>
//                 <Slider style={{}} minimumTrackTintColor={'#f4f3f4'} maximumTrackTintColor={'#323232'} maximumValue={song.duration} value={position}
//                         thumbTintColor={'#f4f3f4'}
//                         thumbStyle={{height: 10, width: 10}}/>
//                 <View style={{flex:1,flexDirection:'row',marginTop:18}}>
//                     <View style={{flex:2,alignItems:'center',justifyContent:'center'}}>
//                         <Icon name="angle-double-left" size={50} color="#f4f3f4"/>
//                     </View>
//                     <View style={{flex:3,alignItems:'center',justifyContent:'center'}}>
//                         <Icon name={playerState===TrackPlayer.STATE_PLAYING?"pause-circle":"play-circle"} size={80} color="#f4f3f4" onPress={()=>{togglePlayPause()}}/>
//                     </View>
//                     <View style={{flex:2,alignItems:'center',justifyContent:'center'}}>
//                         <Icon name="angle-double-right" size={50} color="#f4f3f4"/>
//                     </View>
//
//                 </View>
//             </View>
//         </View>
//     );
// };
//
// export default Player;
