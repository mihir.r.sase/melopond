import React, {Component} from "react";
import {View, Text, Image, TextInput} from 'react-native';
import {Button, IconButton} from 'react-native-paper';
import useAxios from 'axios-hooks';
import ApiRoutes from '../utils/ApiRoutes';
import useLoading from '../utils/useLoading';



const ChangePassword=(props)=>{
    const [state, setState] = React.useState({
        old_password: '',
        password: '',
        password_confirmation:''
    });
    const [error,setError]= React.useState('');

    const handleChange = (value, name) => {
        setState(prevState => ({...prevState, [name]: value}));
    };
    const [{data, loading, error1}, execute] = useAxios({url: ApiRoutes.change_password, method: 'POST'}, {manual: true});
    useLoading(loading);

    React.useLayoutEffect(() => {
        props.navigation.setOptions({
            headerLeft: () => (
                <IconButton icon="menu" color={'#000'} size={25} onPress={() =>{props.navigation.openDrawer();} }/>
            ),
        });
    });

    const changePassword=()=>{
        execute({data:state}).then(data=>{
            setState({old_password:'',password:'',password_confirmation:''})
        }
        ).catch(e=>console.log(e));
    };
    return(
        <View style={{flex:1,backgroundColor:'#fff',padding:18}}>
            <TextInput onChangeText={text => handleChange(text, 'old_password')} value={state.old_password}
                style={{marginTop: 12,  backgroundColor: '#f8f8f8', paddingStart: 12 ,fontSize:16, height: 65}} placeholder={'Old Password'} secureTextEntry={true}/>
            <TextInput onChangeText={text => handleChange(text, 'password')} value={state.password}
                style={{marginTop: 12,  backgroundColor: '#f8f8f8', paddingStart: 12 ,fontSize:16, height: 65}} placeholder={'New Password'} secureTextEntry={true}/>
            <TextInput onChangeText={text => handleChange(text, 'password_confirmation')} value={state.password_confirmation}
                style={{marginTop: 12, backgroundColor: '#f8f8f8', paddingStart: 12 ,fontSize:16, height: 65}} placeholder={'Confirm Password'} secureTextEntry={true}/>
            <Button style={{height: 50, justifyContent: 'center', marginTop: 22}} mode="contained"
                    onPress={() => changePassword()} color={'#33CCFF'} dark={true}>Change Password</Button>
        </View>
    )
};

export default ChangePassword;
