import React, {Component} from 'react';
import {FlatList, Image, Linking, Text, View} from 'react-native';
import {Card, IconButton} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';
import Message from './Message';
import useAxios from 'axios-hooks';
import ApiRoutes from '../utils/ApiRoutes';
import useLoading from '../utils/useLoading';
import AppConstants from '../utils/AppConstants';
import AppRoutes from '../utils/AppRoutes';



const Subscribe =(props)=>{
    const [subscribeList,setSubscribeList]= React.useState();

    const [{data, loading, error}, execute] = useAxios({
        url: ApiRoutes.getSubscriptionPlans,
        method: 'POST',
    }, {manual: true});
    useLoading(loading);

    const [{data1, loading1, error1}, execute1] = useAxios({
        url: ApiRoutes.selectSubscription,
        method: 'POST',
    }, {manual: true});
    useLoading(loading);

    React.useEffect(() => {
        if(loading===false && data===undefined){
            execute().then((data)=>{
                setSubscribeList(data.data.plans);
            }).catch(e=>console.log(e));
        }
    });

    React.useLayoutEffect(() => {
        props.navigation.setOptions({
            headerLeft: () => (
                <IconButton icon="menu" color={'#000'} size={25} onPress={() =>{props.navigation.openDrawer();} }/>
            ),
            headerRight: () => (
                <IconButton icon="history" color={'#000'} size={25} onPress={() =>{props.navigation.navigate(AppRoutes.subscribeHistory);} }/>
            ),
        });
    });

    const selectPlan=(id)=>{
        execute1({data:{plan_id:id}}).then((data)=>{
            Linking.openURL(AppConstants.baseUrlForPayment+data.data.id+"/"+data.data.type).catch(err => console.error("Couldn't load page", err));
        }).catch((e)=>{console.log(e)});
    };

    function Item({ item }) {
        return (
            <Card style={{backgroundColor: '#fff', margin: 8, height: 220}} onPress={()=>selectPlan(item.id)}
                  elevation={8}>
                <View style={{flex: 1}}>
                    <View style={{flexDirection: 'row'}}>
                        <View style={{flex: 8, padding: 18}}>
                            <Text style={{fontSize: 18, fontWeight: 'bold'}}>{item.title}</Text>
                        </View>
                        <View style={{flex: 12, backgroundColor: '#03a9f4'}}>
                            <View style={{flexDirection: 'row', flex: 1, justifyContent: 'center'}}>
                                <View style={{justifyContent: 'center',flex:1}}>
                                    <Text style={{fontSize: 16, fontWeight: 'bold', color: '#fff', marginStart: 8}}>{item.price} for {item.duration}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={{padding:18}}>
                        <View style={{flexDirection:'row',alignItems:'center',marginTop:20}}>
                            <Icon name="shield" size={25} color="#33CCFF" style={{flex:1}} />
                            <Text style={{fontSize:16,marginStart:8,flex:9}}>No Advertisements</Text>
                        </View>
                        <View style={{flexDirection:'row',alignItems:'center',marginTop:20}}>
                            <Icon name="tags" size={25} color="#33CCFF" style={{flex:1}} />
                            <Text style={{fontSize:16,marginStart:8,flex:9}}>Get ₹15,000 coupons for hiring Musicians,
                                upto 20% Discount.</Text>
                        </View>
                    </View>
                </View>
            </Card>
        );
    }

    return (
        <View style={{flex: 1, backgroundColor: '#fff', padding: 12}}>


            <FlatList
                style={{marginTop: 8}}
                data={subscribeList}
                renderItem={({item}) => <Item item={item}/>}
                keyExtractor={item => item.slug}
            />


        </View>
    );
};
export default Subscribe;
