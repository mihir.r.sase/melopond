import React, {Component} from 'react';
import {FlatList, View} from 'react-native';
import Song from './Song';
import {IconButton} from 'react-native-paper';


let TRACK_DATA = [
    {
        id: '1',
        name: 'Tera Jaisa Yaar Kaha',
        album: 'Yaarana',
        genre: 'Traditional',
        length: '03:06',
    },
    {
        id: '2',
        name: 'Neele Neele Ambar par',
        album: 'Kalaakaar',
        genre: 'Traditional',
        length: '03:20',
    },
    {
        id: '3',
        name: 'Tujhse Naaraz Nahi Zindagi',
        album: 'Masoom',
        genre: 'Romantic',
        length: '04:02',
    },
    {
        id: '4',
        name: 'Tera Jaisa Yaar Kaha',
        album: 'Yaarana',
        genre: 'Traditional',
        length: '03:06',
    },
    {
        id: '5',
        name: 'Neele Neele Ambar par',
        album: 'Kalaakaar',
        genre: 'Traditional',
        length: '03:20',
    },
    {
        id: '6',
        name: 'Tujhse Naaraz Nahi Zindagi',
        album: 'Masoom',
        genre: 'Romantic',
        length: '04:02',
    },

];
const SongList = (props) => {


    React.useLayoutEffect(() => {
        props.navigation.setOptions({
            title: props.route.params.title,
        });
    });
console.log(props.route.params);
    return (
        <View style={{backgroundColor: '#fff', padding: 18,flex:1}}>
            <FlatList
                style={{marginTop: 8}}
                data={props.route.params.songs}
                renderItem={({item}) => <Song item={item}/>}
                keyExtractor={item => item.slug}
            />
        </View>
    );
};
export default SongList;
