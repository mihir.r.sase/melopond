import useAxios from 'axios-hooks';
import ApiRoutes from '../utils/ApiRoutes';
import useLoading from '../utils/useLoading';
import React from 'react';
import {useFocusEffect} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import AppConstants from '../utils/AppConstants';
import {Button, Card, IconButton} from 'react-native-paper';
import AppRoutes from '../utils/AppRoutes';
import Icon from 'react-native-vector-icons/FontAwesome';
import WebView from 'react-native-webview';
import {FlatList, Image, ScrollView, Text, TextInput, View} from 'react-native';
import Song from './Song';

const MusicianView = (props) => {
    const [profileData, setProfileData] = React.useState('');

    const [{data, loading, error}, execute] = useAxios({
        url: ApiRoutes.musicianView,
        method: 'POST',
    }, {manual: true});
    useLoading(loading);

    const [{data1, loading1, error1}, execute1] = useAxios({
        url: ApiRoutes.musicianLikeProfile,
        method: 'POST',
    }, {manual: true});


    useFocusEffect(
        React.useCallback(() => {
            // Do something when the screen is focused
            if (loading === false && data === undefined) {
                execute({data:{slug:props.route.params.slug}}).then((data) => {
                    setProfileData(data.data);
                }).catch(e => console.log(e));
            }
        }, []),
    );



    const likeProfile=()=>{
      execute1({data:{musician_id:profileData.profile.id}}).then(data=>{
          console.log(data.data);
      }).catch(e=>console.log(e));
    };


    function Video({ item}) {

        return (
            <Card style={{ backgroundColor:'#fff',margin:6}}
                  elevation={8}>
                <WebView
                    style={{ width: 320, height: 230 }}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    source={{ uri: 'https://www.youtube.com/embed/'+item.url }}
                />

            </Card>
        );
    }
    //"https://www.youtube.com/embed/-ZZPOXn6_9w"

    return (
        <ScrollView>
            <View style={{backgroundColor: '#fff', padding: 18}}>

                <View style={{flexDirection: 'row', flex: 1}}>
                    <View style={{justifyContent: 'center'}}>
                        <Image source={{uri: profileData.profile?.profile_pic}}
                               style={{height: 100, width: 100, borderRadius: 100 / 2}}/>
                    </View>
                    <View style={{flex: 1, marginStart: 28}}>
                        <Text style={{fontSize: 20, fontWeight: 'bold'}}>{profileData.profile?.name}</Text>
                        <Text style={{fontSize: 14, marginTop: 8}}>{profileData.profile?.about_me}</Text>
                    </View>
                </View>
             <Button style={{height: 50, justifyContent: 'center', marginTop: 28}} mode="contained" onPress={() => likeProfile()} color={'#33CCFF'} dark={true}>Follow</Button>

                <View style={{flexDirection: 'row', padding: 8, marginTop: 28, backgroundColor: '#f8f8f8'}}>
                    <View style={{flex: 1, padding: 8}}>
                        <Text style={{
                            textAlign: 'center',
                            color: '#33CCFF',
                            fontSize: 18,
                            fontWeight: 'bold',
                        }}>{profileData.profile?.total_like}</Text>
                        <Text style={{textAlign: 'center', fontSize: 14, marginTop: 4}}>Likes</Text>
                    </View>
                    <View style={{
                        flex: 1,
                        padding: 8,
                        borderRightColor: '#323232',
                        borderRightWidth: 1,
                        borderLeftColor: '#323232',
                        borderLeftWidth: 1,
                    }}>
                        <Text style={{
                            textAlign: 'center',
                            color: '#33CCFF',
                            fontSize: 18,
                            fontWeight: 'bold',
                        }}>{profileData.profile?.follower}</Text>
                        <Text style={{textAlign: 'center', fontSize: 14, marginTop: 4}}>Followers</Text>
                    </View>
                    <View style={{flex: 1, padding: 8}}>
                        <Text style={{
                            textAlign: 'center',
                            color: '#33CCFF',
                            fontSize: 18,
                            fontWeight: 'bold',
                        }}>₹{profileData.profile?.charges}</Text>
                        <Text style={{textAlign: 'center', fontSize: 14, marginTop: 4}}>Charges</Text>
                    </View>
                </View>


                <View style={{marginTop: 28}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Text style={{fontSize: 18, color: '#33CCFF', fontWeight: 'bold'}}>Tracks</Text>
                        <Text style={{fontSize: 14, textAlign: 'right', flex: 1}}
                              onPress={() => {props.navigation.navigate(AppRoutes.songList, {songs: profileData?.music, title: 'Tracks',});}}
                        >View All</Text>
                    </View>
                    <FlatList
                        style={{marginTop: 8}}
                        data={profileData?.music}
                        renderItem={({item}) => <Song item={item}/>}
                        keyExtractor={item => item.id}
                    />
                </View>

                <Text style={{fontSize: 18, color: '#33CCFF', fontWeight: 'bold'}}>Youtube Videos</Text>
                <FlatList
                    style={{marginTop:8,padding:4}}
                    data={profileData?.video}
                    renderItem={({ item,index}) => <Video item={item} index={index} />}
                    keyExtractor={item => item.link}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                />


            </View>
        </ScrollView>
    );
};

export default MusicianView;
