import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import Logo1 from '../assets/images/logo1.png';
import useAxios from 'axios-hooks';
import ApiRoutes from '../utils/ApiRoutes';
import AsyncStorage from '@react-native-community/async-storage';
import AppConstants from '../utils/AppConstants';
import AppRoutes from '../utils/AppRoutes';
import WebService from '../utils/Webservice';
import {CommonActions} from '@react-navigation/native';


const Splash = (props) => {

    const [{data, loading, error}, execute] = useAxios({url: ApiRoutes.getPreference, method: 'POST'}, {manual: true});


    React.useEffect(() => {
        console.log('In Use Effect');

        if (loading === false && !data && !error) {
            setTimeout(() => {
                AsyncStorage.getItem(AppConstants.auth).then((value) => {
                    if (value !== null) {
                        // value previously stored
value=JSON.parse(value);
                        execute().then((data) => {
                            console.log('My Data' + JSON.stringify(data.data));

                            if (data.data.role_type === false) {
                                props.navigation.dispatch(
                                    CommonActions.reset({
                                        index: 1,
                                        routes: [
                                            { name: AppRoutes.joinAs }
                                        ],
                                    })
                                );


                            } else if (data.data.gen_setup === false || data.data.lang_setup === false) {
                                props.navigation.dispatch(
                                    CommonActions.reset({
                                        index: 1,
                                        routes: [
                                            { name: AppRoutes.preferences,params: {role: value.role}}
                                        ],
                                    })
                                );
                            } else {
                                props.navigation.dispatch(
                                    CommonActions.reset({
                                        index: 1,
                                        routes: [
                                            { name: AppRoutes.explore ,params: {role: value.role}}
                                        ],
                                    })
                                );
                            }

                        }).catch((e) => {
                            console.log("Error: "+e);
                        });

                    } else {
                        props.navigation.dispatch(
                            CommonActions.reset({
                                index: 1,
                                routes: [
                                    { name: AppRoutes.login }
                                ],
                            })
                        );
                    }
                }).catch((e) => {
                    console.log(e);
                });
                console.log('In set timeout');
            }, 2000);
        }

    });
    console.log('RENDER');
    return (
        <View style={{flex: 1, backgroundColor: '#fff', alignItems: 'center'}}>

            <View style={{flex: 1, justifyContent: 'center'}}>
                <Image source={Logo1} style={{height: 160, width: 200}}/>
            </View>
            <View style={{justifyContent: 'flex-end', marginBottom: 50}}>
                <Text style={{fontSize: 14, fontWeight: 'bold', textAlign: 'center'}}>MELOPOND MUSIC LLC.</Text>
                <Text style={{fontSize: 14, fontWeight: 'bold', textAlign: 'center'}}>VERSION 1.0</Text>
            </View>

        </View>
    );
};
export default Splash;
