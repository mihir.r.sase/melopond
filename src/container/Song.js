import {Text, View} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import AppRoutes from '../utils/AppRoutes';

function Song({ item,navigation }) {

    const calculateDuration=(totalSeconds)=>{
        let m = Math.floor(totalSeconds % 3600 / 60);
        let s = Math.floor(totalSeconds % 3600 % 60);
        return (m > 0 && m < 10 ? '0' + m : m) + ':' + (s < 10 ? '0' + s : s)
    };

    return (
        <View style={{ backgroundColor: '#fff',margin:4,paddingBottom:6,paddingTop:6,flexDirection:'row'}}>
            <View style={{flex:4,justifyContent:'center'}}>
                <Text style={{fontSize:14,fontWeight:'bold'}}>{item.title}</Text>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                    <Text style={{fontSize:12,color:'#323232'}}>{item.name}</Text>

                </View>
            </View>
            <View style={{flex:1,justifyContent:'center'}}>
                <View style={{alignItems:'center'}}>
                    <Icon name="play-circle" size={25} color="#33CCFF" onPress={()=>navigation.navigate(AppRoutes.player,{item})}/>
                </View>
                <Text style={{fontSize:10,textAlign:'center'}}>{calculateDuration(item.duration)}</Text>
            </View>
        </View>
    );
}
export default Song;
