const AppConstants = {
    auth: 'auth',
    pref:'preferences',
    baseUrlForPayment:'https://test-api.melopond.com/payment/'
};
export default AppConstants;
