import React, {Component} from 'react';
import {Image, Text, View, TextInput, StyleSheet} from 'react-native';
import Logo from '../assets/images/logo.png';
import {Card, Button, FAB} from 'react-native-paper';
import useAxios from 'axios-hooks';
import ApiRoutes from '../utils/ApiRoutes';
import userLoading from '../utils/useLoading';
import AsyncStorage from '@react-native-community/async-storage';
import AppConstants from '../utils/AppConstants';
import AppRoutes from '../utils/AppRoutes';
import WebService from '../utils/Webservice';
import {CommonActions} from '@react-navigation/native';
import auth from '@react-native-firebase/auth';
import { GoogleSignin } from '@react-native-community/google-signin';
import { LoginManager, AccessToken } from 'react-native-fbsdk';

const Login = (props) => {

    console.log(props.route.params?.email);


    const [state, setState] = React.useState({
        email: '',
        password: '',
    });

    React.useEffect(() => {
        if (props.route.params?.email) {
            handleChange(props.route.params.email, 'email');
        }
    }, [props.route.params?.email]);

    const handleChange = (value, name) => {
        setState(prevState => ({...prevState, [name]: value}));
    };


    const [{data, loading, error}, execute] = useAxios({url: ApiRoutes.login, method: 'POST'}, {manual: true});
    const [{data1, loading1, error1}, execute1] = useAxios({url: ApiRoutes.getPreference, method: 'POST'}, {manual: true});
    const [{data2, loading2, error2}, execute2] = useAxios({url: ApiRoutes.googleLogin, method: 'POST'}, {manual: true});

    const performLogin = () => {
        execute({data: {'email': state.email, 'password': state.password}}).then((data1) => {
            AsyncStorage.setItem(AppConstants.auth, JSON.stringify(data1.data)).then(() => {

                console.log("Execute");
                WebService.init(data1.data.token);
                execute1().then((data) => {
                    if (data.data.role_type === false) {
                        props.navigation.dispatch(
                            CommonActions.reset({
                                index: 1,
                                routes: [
                                    { name: AppRoutes.joinAs }
                                ],
                            })
                        );
                    } else if (data.data.gen_setup === false) {
                        props.navigation.dispatch(
                            CommonActions.reset({
                                index: 1,
                                routes: [
                                    { name: AppRoutes.preferences,params: {role: data1.data.role} }
                                ],
                            })
                        );
                    } else {
                        props.navigation.dispatch(
                            CommonActions.reset({
                                index: 1,
                                routes: [
                                    { name: AppRoutes.explore,params: {role: data1.data.role} }
                                ],
                            })
                        );
                    }
                }).catch((e) => {
                    console.log("Error: "+e);
                });

            }).catch((e) => console.log(e));
        }).catch((e) => console.log(e));
    };

    userLoading(loading);



    React.useEffect(() => {

        GoogleSignin.configure({
            webClientId: '539991633572-ktm5gpugbectrot4ic605qtuplbtnb0j.apps.googleusercontent.com', // From Firebase Console Settings
        });


    }, []);

    async function onGoogleButtonPress() {

        // Get the users ID token
        const { idToken } = await GoogleSignin.signIn();

        // Create a Google credential with the token
        const googleCredential = auth.GoogleAuthProvider.credential(idToken);
        // Sign-in the user with the credential
        return auth().signInWithCredential(googleCredential);
    }

    const googleLogin=(user,type)=>{
        execute2({data:{profile:JSON.stringify(user.additionalUserInfo.profile),phone_number:user.user.phoneNumber,unique_id:user.user.uid,type:type}}).then((data1) => {
            AsyncStorage.setItem(AppConstants.auth, JSON.stringify(data1.data)).then(() => {

                console.log("Execute");
                WebService.init(data1.data.token);
                execute1().then((data) => {
                    if (data.data.role_type === false) {
                        props.navigation.dispatch(
                            CommonActions.reset({
                                index: 1,
                                routes: [
                                    { name: AppRoutes.joinAs }
                                ],
                            })
                        );
                    } else if (data.data.gen_setup === false) {
                        props.navigation.dispatch(
                            CommonActions.reset({
                                index: 1,
                                routes: [
                                    { name: AppRoutes.preferences,params: {role: data1.data.role} }
                                ],
                            })
                        );
                    } else {
                        props.navigation.dispatch(
                            CommonActions.reset({
                                index: 1,
                                routes: [
                                    { name: AppRoutes.explore,params: {role: data1.data.role} }
                                ],
                            })
                        );
                    }
                }).catch((e) => {
                    console.log("Error: "+e);
                });

            }).catch((e) => console.log(e));
        }).catch((e) => console.log(e));
    };

    async function onFacebookButtonPress() {
        // Attempt login with permissions
        const result = await LoginManager.logInWithPermissions(['public_profile', 'email']);

        if (result.isCancelled) {
            throw 'User cancelled the login process';
        }

        // Once signed in, get the users AccesToken
        const data = await AccessToken.getCurrentAccessToken();

        if (!data) {
            throw 'Something went wrong obtaining access token';
        }

        // Create a Firebase credential with the AccessToken
        const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken);

        // Sign-in the user with the credential
        return auth().signInWithCredential(facebookCredential);
    }

    return (
        <View style={{flex: 1, backgroundColor: '#fff'}}>


            <View style={{marginTop: 20, flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Image source={Logo} style={{height: 60, width: 120}}/>
            </View>

            <View style={{flex: 5, padding: 12}}>
                <Card style={{flex: 1, padding: 12, backgroundColor: '#fff'}}
                      elevation={8}>

                    <Text style={{fontSize: 18, fontWeight: 'bold', textAlign: 'center'}}>Login</Text>
                    <TextInput onChangeText={text => handleChange(text, 'email')} value={state.email}
                               style={{
                                   fontSize: 16,
                                   height: 65,
                                   backgroundColor: '#f8f8f8',
                                   marginTop: 16,
                                   paddingStart: 12,
                               }}
                               placeholder={'Email'}/>
                    <TextInput onChangeText={text => handleChange(text, 'password')} value={state.password}
                               secureTextEntry={true}
                               style={{
                                   fontSize: 16,
                                   height: 65,
                                   backgroundColor: '#f8f8f8',
                                   marginTop: 16,
                                   paddingStart: 12,
                               }}
                               placeholder={'Password'}/>
                    <Text style={{fontSize: 14, fontWeight: 'bold', textAlign: 'right', marginTop: 14}}   onPress={() => props.navigation.navigate(AppRoutes.forgotPassword)}>Forgot
                        password?</Text>
                    <Button style={{height: 50, justifyContent: 'center', marginTop: 16}} mode="contained"
                            onPress={() => performLogin()}
                            color={'#33CCFF'} dark={true}>Login</Button>
                    <Text style={{fontSize: 12, textAlign: 'center', marginTop: 16, marginBottom: 8}}>Or login
                        with</Text>
                    <View style={{alignItems: 'center'}}>
                        <View style={{flexDirection: 'row'}}>
                            <FAB style={{backgroundColor: '#f6f6f6', marginEnd: 6}} icon="google"
                                 onPress={() => {
                                     onGoogleButtonPress().then((user) => googleLogin(user,3)).catch(e => console.log(e))
                                 }}/>
                            <FAB style={{backgroundColor: '#475993', marginStart: 6}} icon="facebook"
                                 onPress={() => onFacebookButtonPress().then((user) => {
                                     googleLogin(user,2)
                                 })} color={'#fff'}/>
                        </View>

                    </View>
                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <Text onPress={() => props.navigation.navigate(AppRoutes.register)} style={{
                            fontSize: 14,
                            fontWeight: 'bold',
                            textAlign: 'center',
                            color: '#33CCFF',
                            marginTop: 12,
                        }}>Do not have an account? Join Melopond.</Text>
                    </View>
                </Card>
            </View>

        </View>
    );
};

export default Login;
