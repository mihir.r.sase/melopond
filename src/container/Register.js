import React, {Component} from 'react';
import {Image, Text, TextInput, View} from 'react-native';

import {Button, Card, FAB, HelperText} from 'react-native-paper';
import Logo from '../assets/images/logo.png';
import useAxios from 'axios-hooks';
import ApiRoutes from '../utils/ApiRoutes';
import userLoading from '../utils/useLoading';
import AppRoutes from '../utils/AppRoutes';

const Register = (props) => {
    const [state, setState] = React.useState({
        email: '',
        password: '',
        firstName: '',
        lastName: '',
        errorEmail: '',
        errorPassword: '',
        errorFirst: '',
        errorLast: '',
    });

    const handleChange = (value, name) => {
        setState(prevState => ({...prevState, [name]: value}));
    };

    const [{data, loading, error}, execute] = useAxios({url: ApiRoutes.register, method: 'POST'}, {manual: true});
    console.log({'Data': JSON.stringify(data), 'loading': loading, 'error': error});
    userLoading(loading);

    const performRegistration = () => {
        execute({data: {'first_name':state.firstName,'last_name':state.lastName,'email': state.email, 'password': state.password}}).then(()=>{
            props.navigation.navigate(AppRoutes.login,{email:state.email});
        }).catch(e=>console.log(e));
    };



    return (
        <View style={{flex: 1, backgroundColor: '#fff'}}>


            <View style={{marginTop: 20, flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Image source={Logo} style={{height: 60, width: 120}}/>
            </View>

            <View style={{flex: 8, padding: 12}}>
                <Card style={{flex: 1, padding: 12, backgroundColor: '#fff',justifyContent:'center'}}
                      elevation={8}>

                    <Text style={{fontSize: 18, fontWeight: 'bold', textAlign: 'center',marginTop:20}}>Join Melopond</Text>
                    <View style={{flexDirection: 'row', marginTop: 8}}>

                        <View style={{flex: 1, marginEnd: 4}}>
                            <TextInput style={{
                                marginTop: 2,
                                fontSize: 16,
                                height: 65,
                                backgroundColor: '#f8f8f8',
                                paddingStart: 12
                            }} placeholder={'First Name'} onChangeText={text => handleChange(text, 'firstName')}
                                       value={state.firstName}/>
                            <HelperText type="error" visible>
                                {state.errorFirst}
                            </HelperText>
                        </View>
                        <View style={{flex: 1, marginStart: 4}}>
                            <TextInput style={{
                                marginTop: 2,
                                fontSize: 16,
                                height: 65,
                                backgroundColor: '#f8f8f8',
                                paddingStart: 12
                            }} placeholder={'Last Name'} onChangeText={text => handleChange(text, 'lastName')}
                                       value={state.lastName}/>
                            <HelperText type="error" visible>
                                {state.errorLast}
                            </HelperText>
                        </View>



                    </View>
                    <TextInput style={{
                        fontSize: 16,
                        height: 65,
                        backgroundColor: '#f8f8f8',
                        marginTop: 2,
                        paddingStart: 12,
                    }}
                               placeholder={'Email'} onChangeText={text => handleChange(text, 'email')}
                               value={state.email}/>
                    <HelperText type="error" visible>
                        {state.errorEmail}
                    </HelperText>
                    <TextInput style={{
                        fontSize: 16,
                        height: 65,
                        backgroundColor: '#f8f8f8',
                        marginTop: 2,
                        paddingStart: 12,
                    }} secureTextEntry={true}
                               placeholder={'Password'} onChangeText={text => handleChange(text, 'password')}
                               value={state.password}/>
                    <HelperText type="error" visible>
                        {state.errorPassword}
                    </HelperText>
                    <Button style={{height: 50, justifyContent: 'center', marginTop: 8}} mode="contained"
                            onPress={() => performRegistration()}
                            color={'#33CCFF'} dark={true}>Register</Button>

                </Card>
            </View>


        </View>
    );
};
export default Register;
