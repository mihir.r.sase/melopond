import React, {Component} from "react";
import {View, Text, Image, TextInput, ScrollView, TouchableOpacity} from 'react-native';
import Select from '../../utils/Dropdown';
import Arijit from '../../assets/images/arijit.png';
import {Button} from 'react-native-paper';
import useAxios from 'axios-hooks';
import ApiRoutes from '../../utils/ApiRoutes';
import useLoading from '../../utils/useLoading';
import ImagePicker from 'react-native-image-picker';


const MUSICIAN_TYPE =[
    {label:'Musician',value:0},{label:'Band',value:1}
];


const MusicianEditProfile = (props) => {

    const [{data, loading, error}, execute] = useAxios({
        url: ApiRoutes.getEventCategories,
        method: 'POST',
    }, {manual: true});
    const [{data1, loading1, error1}, execute1] = useAxios({url: ApiRoutes.updateProfile, method: 'POST'}, {manual: true});
    const [{data2, loading2, error2}, execute2] = useAxios({url: ApiRoutes.updateProfilePic, method: 'POST'}, {manual: true});
    useLoading(loading1);
    const [profileData, setProfileData] = React.useState(props.route.params);
    const [profilePic, setProfilePic] = React.useState({uri: profileData?.profile_pic});
    const [eventCategories,setEventCategories]= React.useState();
    const [state, setState] = React.useState({
        first_name: profileData.first_name,
        last_name: profileData.last_name,
        mobile_no: profileData?.mobile_no,
        about_me: profileData?.about_me,
        city: profileData?.city,
        charges:profileData?.charges,
        musician_type:profileData?.musician_type==='Musician'?0:1,
        events:''
    });




    React.useEffect(() => {
        if(loading===false && data===undefined){
                execute().then((data)=>{
                    setEventCategories(data.data);
                })    .catch(e=>console.log(e));
        }
    });

    const handleChange = (value, name) => {
        setState(prevState => ({...prevState, [name]: value}));
    };

    const updateProfile = () => {

        execute1({data: state}).then((data) => {
            props.navigation.goBack();
        }).catch(e => console.log(e));
    };

    const displayImagePicker = () => {
        const options = {
            title: 'Select Avatar',
            customButtons: [{name: 'fb', title: 'Choose Photo from Facebook'}],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.launchImageLibrary(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = {uri: response.uri};

                execute2({data: {profile_pic: response.data}}).then(() => {
                    setProfilePic(source);
                }).catch((e) => console.log(e));


            }
        });
    };


        return(
        <ScrollView>
            <View style={{flex:1,backgroundColor:'#fff',padding:18}}>

                <View>
                    <View style={{justifyContent: 'center',alignItems:'center'}}>
                        <TouchableOpacity onPress={() => displayImagePicker()}>
                        <Image source={profilePic} style={{height: 100, width: 100, borderRadius: 100 / 2}}/>
                        </TouchableOpacity>
                    </View>
                    <Text style={{fontSize:18,fontWeight:'bold',textAlign:'center',marginBottom:18}}>{profileData.name}</Text>
                </View>
                <View style={{flexDirection:'row'}}>
                    <View style={{flex:1,marginEnd:4}}>
                        <Text>First Name</Text>
                        <TextInput onChangeText={text => handleChange(text, 'first_name')} value={state.first_name}
                                   style={{
                                       marginTop: 4,
                                       fontSize: 16,
                                       height: 65,
                                       backgroundColor: '#f8f8f8',
                                       paddingStart: 12,
                                   }}/>
                    </View>
                    <View style={{flex:1,marginStart:4}}>
                        <Text>Last Name</Text>
                        <TextInput onChangeText={text => handleChange(text, 'last_name')} value={state.last_name}
                                   style={{
                                       marginTop: 4,
                                       fontSize: 16,
                                       height: 65,
                                       backgroundColor: '#f8f8f8',
                                       paddingStart: 12,
                                   }}/>
                    </View>
                </View>
                <Text style={{marginTop:8}}>About Me</Text>
                <TextInput onChangeText={text => handleChange(text, 'about_me')} value={state.about_me}
                           style={{
                               marginTop: 4,
                               fontSize: 16,
                               height: 85,
                               backgroundColor: '#f8f8f8',
                               paddingStart: 12,
                           }}
                           multiline={true} numberOfLines={5}/>
                <Text style={{marginTop:8}}>Mobile Number</Text>
                <TextInput onChangeText={text => handleChange(text, 'mobile_no')} value={state.mobile_no}
                           style={{
                               marginTop: 4,
                               fontSize: 16,
                               height: 65,
                               backgroundColor: '#f8f8f8',
                               paddingStart: 12,
                           }} keyboardType={'numeric'}/>
                <Text style={{marginTop:8}}>Hire Charges(₹)</Text>
                <TextInput onChangeText={text => handleChange(text, 'charges')} value={'₹'+state.charges}
                           style={{
                               marginTop: 4,
                               fontSize: 16,
                               height: 65,
                               backgroundColor: '#f8f8f8',
                               paddingStart: 12,
                           }} keyboardType={'numeric'}/>
                <Text style={{marginTop:8}}>Musician Type</Text>
                <Select data={MUSICIAN_TYPE} placeholder='Musician Type' value={state.musician_type} onChangeText={(value) => {handleChange(value,'musician_type');}} />
                <Text style={{marginTop:8}}>Location</Text>
                <TextInput onChangeText={text => handleChange(text, 'city')} value={state.city}
                           style={{
                               marginTop: 4,
                               fontSize: 16,
                               height: 65,
                               backgroundColor: '#f8f8f8',
                               paddingStart: 12,
                           }} placeholder={'Pune'}/>
                <Text style={{marginTop:8}}>Event Type</Text>
                <Select data={eventCategories} placeholder='Event Category' value={state.events} onChangeText={(value) => {handleChange(value,'events');}} />
                <Button style={{height: 50, justifyContent: 'center', marginTop: 22}} mode="contained"
                        onPress={() => updateProfile()} color={'#33CCFF'} dark={true}>Save Changes</Button>
            </View>
        </ScrollView>
    )
};

export default MusicianEditProfile;
