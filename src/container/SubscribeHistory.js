import React from 'react';
import useAxios from 'axios-hooks';
import ApiRoutes from '../utils/ApiRoutes';
import useLoading from '../utils/useLoading';
import {Card, IconButton} from 'react-native-paper';
import {FlatList, Linking, Text, View} from 'react-native';
import AppConstants from '../utils/AppConstants';
import Icon from 'react-native-vector-icons/FontAwesome';

const SubscribeHistory =(props)=>{
    const [subscribeList,setSubscribeList]= React.useState();

    const [{data, loading, error}, execute] = useAxios({
        url: ApiRoutes.getSubscriptionHistory,
        method: 'POST',
    }, {manual: true});
    useLoading(loading);

    React.useEffect(() => {
        if(loading===false && data===undefined){
            execute().then((data)=>{
                setSubscribeList(data.data.list);
            }).catch(e=>console.log(e));
        }
    });




    function Item({ item }) {
        return (
            <Card style={{backgroundColor: '#fff', margin: 8,padding:12}} onPress={()=>selectPlan(item.id)}
                  elevation={8}>
                <View style={{flex: 1}}>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{flex:1}}>Title</Text>
                        <Text style={{flex:1,textAlign:'right',fontWeight:'bold'}}>{item.title}</Text>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{flex:1}}>Status</Text>
                        <Text style={{flex:1,textAlign:'right',fontWeight:'bold'}}>{item.status}</Text>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{flex:1}}>Duration</Text>
                        <Text style={{flex:1,textAlign:'right',fontWeight:'bold'}}>{item.duration}</Text>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{flex:1}}>Price</Text>
                        <Text style={{flex:1,textAlign:'right',fontWeight:'bold'}}>{item.price}</Text>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{flex:1}}>Created At</Text>
                        <Text style={{flex:1,textAlign:'right',fontWeight:'bold'}}>{item.created_at}</Text>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{flex:1}}>Expiry Date</Text>
                        <Text style={{flex:1,textAlign:'right',fontWeight:'bold'}}>{item.expiry_date}</Text>
                    </View>
                </View>
            </Card>
        );
    }

    return (
        <View style={{flex: 1, backgroundColor: '#fff', padding: 12}}>


            <FlatList
                style={{marginTop: 8}}
                data={subscribeList}
                renderItem={({item}) => <Item item={item}/>}
                keyExtractor={item => item.id.toString()}
            />


        </View>
    );
};
export default SubscribeHistory;
