echo "Resetting Project"

./clean_gradle.sh

cd ..

rm -rf node_modules

echo "Clear NPM Cache"
npm cache clear --force

echo "NPM Install"
npm install

echo "Completed!"
