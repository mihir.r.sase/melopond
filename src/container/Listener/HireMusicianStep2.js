import {ScrollView, Text, TextInput, TouchableOpacity, View} from 'react-native';
import Select from '../../utils/Dropdown';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Button} from 'react-native-paper';
import React, {Component} from 'react';
import useAxios from 'axios-hooks';
import ApiRoutes from '../../utils/ApiRoutes';
import useLoading from '../../utils/useLoading';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import AppRoutes from '../../utils/AppRoutes';

const HireMusicianStep2 = (props) => {

    const [eventCategories,setEventCategories]= React.useState();
    const [state, setState] = React.useState({
        date: '',
        category_id:'',
        budget:'',
        location:'',
        name:'',
        mobile_no:'',
        message:'',
        slug:props.route.params.slug,
        code:'',
        disAmt:''
    });
    const handleChange = (value, name) => {
        setState(prevState => ({...prevState, [name]: value}));
    };

    const [{data, loading, error}, execute] = useAxios({
        url: ApiRoutes.getEventCategories,
        method: 'POST',
    }, {manual: true});

    const [{data1, loading1, error1}, execute1] = useAxios({
        url: ApiRoutes.getHireMusicianCost,
        method: 'POST',
    }, {manual: true});

    const [{data2, loading2, error2}, execute2] = useAxios({
        url: ApiRoutes.hireMusician,
        method: 'POST',
    }, {manual: true});


    React.useEffect(() => {

        if (loading === false && data === undefined) {
            execute().then((data)=>{
                execute1({data:{slug:state.slug}}).then((data1)=>{
                    handleChange(data1.data[0],'budget')
                }).catch(e=>console.log(e));
                setEventCategories(data.data)
            }).catch(e=>console.log(e));
        }


    });

    const [isDatePickerVisible, setDatePickerVisibility] = React.useState(false);

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    const handleConfirm = date => {

        handleChange(date,'date');
        hideDatePicker();
    };


    const hireMusician=()=>{
        execute2({data:{event_category_id:state.category_id,date:state.date,budget:state.budget,
                location:state.location,name:state.name,mobile_no:state.mobile_no,message:state.message,
                slug:state.slug,code:state.code,disAmt:state.disAmt}}).then(()=>{
                    props.navigation.goBack();
        }).catch(e=>{console.log(e)})
    };

    return (
        <ScrollView>
            <View style={{flex: 1, backgroundColor: '#fff', padding: 12}}>
                <DateTimePickerModal
                    isVisible={isDatePickerVisible}
                    mode="date"
                    onConfirm={handleConfirm}
                    onCancel={hideDatePicker}
                />
                <Select data={eventCategories} placeholder='Event Category' value={state.category_id} onChangeText={(value) => {handleChange(value,'category_id');}} />
                <TextInput onChangeText={text => handleChange(text, 'budget')} value={'₹'+state.budget} editable={false}
                    style={{marginTop: 12,fontSize:16, height: 65, backgroundColor: '#d1d1d1', paddingStart: 12}} placeholder={'Budget(₹)'} keyboardType={'numeric'}/>
                <TouchableOpacity onPress={() =>showDatePicker()}>
                <View style={{marginTop: 12, height: 65, backgroundColor: '#f8f8f8', paddingStart: 12,justifyContent:'center'}}>
                         <Text style={state.date===''?{color:'#adadad',fontSize:16}:{fontSize:16}} >{state.date===''?'Date':state.date.toDateString()}</Text>
                </View>
                </TouchableOpacity>

                <TextInput onChangeText={text => handleChange(text, 'location')} value={state.location}
                    style={{marginTop: 12,fontSize:16, height: 65, backgroundColor: '#f8f8f8', paddingStart: 12}} placeholder={'Location'}/>
                <TextInput onChangeText={text => handleChange(text, 'name')} value={state.name}
                    style={{marginTop: 12,fontSize:16, height: 65, backgroundColor: '#f8f8f8', paddingStart: 12}} placeholder={'Name'}/>
                <TextInput onChangeText={text => handleChange(text, 'mobile_no')} value={state.mobile_no}
                    style={{marginTop: 12,fontSize:16, height: 65, backgroundColor: '#f8f8f8', paddingStart: 12}} placeholder={'Mobile'} keyboardType={'numeric'}/>
                <TextInput onChangeText={text => handleChange(text, 'message')} value={state.message}
                    style={{marginTop: 12,fontSize:16, height: 65, backgroundColor: '#f8f8f8', paddingStart: 12}} placeholder={'Message(Optional)'} multiline={true} numberOfLines={5}/>

                <Button style={{height: 50, justifyContent: 'center', marginTop: 22}} mode="contained"
                        onPress={() => hireMusician()} color={'#33CCFF'} dark={true}>Submit Request</Button>
            </View>
        </ScrollView>
    );
};
export default HireMusicianStep2;
