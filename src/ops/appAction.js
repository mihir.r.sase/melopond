function isLoadingApp(isLoading) {
    return dispatch => {
        dispatch({type: APP_CONSTANTS.LOADING, payload: isLoading});
    };
}

const appActions = {
    isLoadingApp,
};

export default appActions;

export const APP_CONSTANTS = {
    LOADING: 'LOADING',
};
