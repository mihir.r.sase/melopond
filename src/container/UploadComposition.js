import React, {Component} from 'react';
import {Image, Text, TextInput, View, ScrollView} from 'react-native';
import Select from '../utils/Dropdown';
import {Button, Card} from 'react-native-paper';
import CloudUploadOutlined from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/FontAwesome';

class UploadComposition extends Component {

    render() {
        return (
            <ScrollView>
                <View style={{flex: 1, backgroundColor: '#fff', padding: 12}}>
                    <TextInput style={{marginTop: 4, backgroundColor: '#f8f8f8', paddingStart: 12,fontSize:16, height: 65}} placeholder={'Song Name'}/>
                    <Select placeholder='Select Genre' onChangeText={(value) => {
                        this.setState({userID: value});
                    }}/>
                    <Select placeholder='Select Language' onChangeText={(value) => {
                        this.setState({userID: value});
                    }}/>

                    <Select placeholder='Select Feeling' onChangeText={(value) => {
                        this.setState({userID: value});
                    }}/>
                    <View style={{marginTop: 8, height: 150, backgroundColor: '#f8f8f8', justifyContent: 'center'}}>
                        <View style={{alignItems: 'center'}}>
                            <Icon name="cloud-upload" size={50} color="#33CCFF"/>
                        </View>
                        <Text style={{textAlign: 'center', marginTop: 4, fontSize: 14, fontWeight: 'bold'}}>Select
                            File</Text>
                    </View>
                    <Button style={{height: 50, justifyContent: 'center', marginTop: 16}} mode="contained"
                            onPress={() => console.log('Pressed')} color={'#33CCFF'} dark={true}>Upload</Button>
                </View>
            </ScrollView>
        );
    }
}

export default UploadComposition;
