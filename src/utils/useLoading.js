import React, {Component} from 'react';
import appActions from '../ops/appAction';
import {useDispatch} from 'react-redux';

function useLoading(loading: boolean) {
    const dispatch = useDispatch();
    React.useEffect(() => {
        dispatch(appActions.isLoadingApp(loading));
    }, [
        dispatch, loading,
    ]);
}

export default useLoading;
