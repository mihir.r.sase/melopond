import {configure} from 'axios-hooks';
import LRU from 'lru-cache';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import AppConstants from './AppConstants';


const body= (token) => {
    const axios = Axios.create({
        baseURL: 'https://test-api.melopond.com/api',
    });


    axios.interceptors.request.use(function (config) {
        // Do something before request is sent
        if (token !== null) {
            config.headers = {...config.headers, Authorization: `Bearer ${token}`};
        }

        console.log('REQUEST: ' + JSON.stringify(config));
        return config;
    }, function (error) {
        // Do something with request error
        return Promise.reject(error);
    });

// Add a response interceptor
    axios.interceptors.response.use(function (response) {
        // Any status code that lie within the range of 2xx cause this function to trigger
        // Do something with response data
          console.log('RESPONSE: ' + JSON.stringify(response));
        return response;
    }, function (error) {
        // Any status codes that falls outside the range of 2xx cause this function to trigger
        // Do something with response error
        return Promise.reject(error);
    });

    const cache = new LRU({max: 10});
    configure({axios, cache});
};

const WebService = {

    init: (token) => {
        console.log("Token:",token);
        if (token  !==undefined) {
            body(token);
        } else {

            AsyncStorage.getItem(AppConstants.auth).then((value) => {
                value = JSON.parse(value);
                if (value !== null) {
                    body(value.token);
                } else {
                    body();
                }
            });
        }
    },

};

export function getFormData(object) {
    const formData = new FormData();
    Object.keys(object).forEach(key => formData.append(key, object[key]));
    return formData;
}

export default WebService;
