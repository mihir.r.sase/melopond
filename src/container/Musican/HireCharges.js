import React, { Component } from 'react';
//Import React
import { View, Text, Picker, StyleSheet, SafeAreaView ,TextInput} from 'react-native';
//Import basic react native components
import MultiSelect from 'react-native-multiple-select';
import {Button, Card, IconButton} from 'react-native-paper';
import useAxios from 'axios-hooks';
import ApiRoutes from '../../utils/ApiRoutes';
import useLoading from '../../utils/useLoading';
//Import MultiSelect library

//Dummy Data for the MutiSelect





const HireCharges = (props) => {
    const [selectedItems,setSelectedItems]= React.useState();
    const [chargesAndEvents,setChargesAndEvents]= React.useState({
        event:'',
        charges:'',
        categories:''
    });
    const handleChange = (value, name) => {
        setChargesAndEvents(prevState => ({...prevState, [name]: value}));
    };
    const [{data, loading, error}, execute] = useAxios({url: ApiRoutes.getChargesAndEvents, method: 'POST'}, {manual: true});
    const [{data1, loading1, error1}, execute1] = useAxios({url: ApiRoutes.updateChargesAndEvent, method: 'POST'}, {manual: true});
    useLoading(loading);

    const onSelectedItemsChange = (selectedItems) => {
        // do something with selectedItems
        setSelectedItems(selectedItems);
        handleChange(selectedItems,'event');
    };

    React.useEffect(() => {
        if(loading===false && data === undefined) {
            execute().then(data=>{
                setChargesAndEvents({event:data.data.event,charges:data.data.charges,categories:data.data.categories})
            }).catch(e=>console.log(e));
        }
    });

    React.useLayoutEffect(() => {
        props.navigation.setOptions({
            headerLeft: () => (
                <IconButton icon="menu" color={'#000'} size={25} onPress={() =>{props.navigation.openDrawer();} }/>
            ),
        });
    });

    const saveChange=()=>{
        console.log(JSON.stringify(chargesAndEvents));
        execute1({data:{events:chargesAndEvents.event,cost:chargesAndEvents.charges}}).then(()=>{
            console.log("Success");
        }).catch(e=>console.log(e));
    };


    return(
        <View style={{flex: 1, backgroundColor: '#fff', padding: 18}}>
            <Text style={{marginTop: 8}}>Charges</Text>
            <TextInput
                onChangeText={text => handleChange(text, 'charges')} value={chargesAndEvents.charges}
                style={{
                    marginTop: 4,
                    fontSize: 16,
                    height: 65,
                    backgroundColor: '#f8f8f8',
                    paddingStart: 12,
                }} keyboardType={'numeric'}/>
            <Text style={{marginTop: 8,marginBottom:18}}>Events</Text>
            <MultiSelect
                styleInputGroup={{ backgroundColor: '#f8f8f8'}}
                styleDropdownMenuSubsection={{ backgroundColor: '#f8f8f8',height:65}}
                hideTags
                items={chargesAndEvents.categories}
                uniqueKey="value"
                onSelectedItemsChange={onSelectedItemsChange}
                selectedItems={selectedItems}
                selectText="Pick Items"
                searchInputPlaceholderText="Search Items..."
                onChangeInput={(text) => console.log(text)}
                altFontFamily="ProximaNova-Light"
                tagRemoveIconColor="#CCC"
                tagBorderColor="#CCC"
                tagTextColor="#CCC"
                selectedItemTextColor="#CCC"
                selectedItemIconColor="#CCC"
                itemTextColor="#000"
                displayKey="label"
                searchInputStyle={{color: '#CCC'}}
                submitButtonColor="#CCC"
                submitButtonText="Select"
            />
            <Button style={{height: 50, justifyContent: 'center', marginTop: 16}} mode="contained"
                    onPress={() => saveChange()}
                    color={'#33CCFF'} dark={true}>Save</Button>
        </View>
    );
};
export default HireCharges;

