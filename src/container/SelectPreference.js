import React, {Component} from 'react';
import {Image, Text, View, FlatList, ScrollView} from 'react-native';
import {Card, IconButton} from 'react-native-paper';
import Mike from '../assets/images/mike.png';
import HeadPhones from '../assets/images/headphones.png';
import Music from '../assets/images/music.png';
import Sound from '../assets/images/sound.png';
import useAxios from 'axios-hooks';
import ApiRoutes from '../utils/ApiRoutes';
import userLoading from '../utils/useLoading';
import AppRoutes from '../utils/AppRoutes';
import {CommonActions, useFocusEffect} from '@react-navigation/native';



const SelectPreference = (props) => {


    const [languageData, setLanguageData] = React.useState();
    const [genreData, setGenreData] = React.useState();
    const [languageChange, setLanguageChange] = React.useState([]);
    const [genreChange, setGenreChange] = React.useState([]);

    const [{data, loading, error}, execute] = useAxios({url: ApiRoutes.getGenreLanguage, method: 'POST'}, {manual: true});
    const [{data1, loading1, error1}, execute1] = useAxios({url: ApiRoutes.addPreference, method: 'POST'}, {manual: true});
    userLoading(loading);
    useFocusEffect(
        React.useCallback(() => {
            // Do something when the screen is focused
            if(loading === false && data===undefined){
                execute().then((data)=>{
                    setLanguageData(data.data.language);
                    setGenreData(data.data.genres);
                }).catch((e)=>console.log(e));
            }
            return () => {
                // Do something when the screen is unfocused
                // Useful for cleanup functions
            };
        }, []),
    );
    const savePref=()=>{
        let language=[];
        let genre=[];
        for(let i=0;i<languageData.length;i++){
            if(languageData[i].check === 1){
                language.push(languageData[i].id);
            }
        }
        for(let i=0;i<genreData.length;i++){
            if(genreData[i].check === 1){
                genre.push(genreData[i].id);
            }
        }
        console.log({language:language,genre:genre});
        execute1({data: {language:language,genre:genre}}).then(()=>{
            props.navigation.dispatch(
                CommonActions.reset({
                    index: 1,
                    routes: [
                        { name: AppRoutes.explore ,params: {role: props.route.params.role}}
                    ],
                })
            );
        }).catch(e=>console.log(e));

    };

    React.useLayoutEffect(() => {
        props.navigation.setOptions({
            headerRight: () => (
                <IconButton icon="check" color={'#000'} size={25} onPress={() =>savePref() }/>
            ),
        });
    });

    function addOrRemoveFromLanguage(value) {
        console.log(value);

        let index = languageChange.indexOf(value);

        if (index === -1) {
            setLanguageChange(prevState => {
                prevState.push(value);
                return prevState;
            });
        } else {
            setLanguageChange(prevState => {
                prevState.splice(index,1);
                return prevState;
            });

        }
    }
    function addOrRemoveFromGenre(value) {
        console.log(value);

        let index = genreChange.indexOf(value);

        if (index === -1) {
            setGenreChange(prevState => {
                prevState.push(value);
                return prevState;
            });
        } else {
            setGenreChange(prevState => {
                prevState.splice(index,1);
                return prevState;
            });

        }
    }

    const onLanguageChange=(index,item)=>{
        addOrRemoveFromLanguage(item.id);
        setLanguageData(pre => {
            pre[index].check = pre[index].check ===  0 ? 1 : 0;
            return [...pre];
        });
    };


    function LanguageItem({ item,index }) {
        return (
            <Card style={{padding: 12, backgroundColor: item.check===0?'#fff':'#33CCFF',margin:6}} onPress={()=>onLanguageChange(index,item)}
                  elevation={8}>
                <Text style={{color: item.check===0?'#000':'#fff'}}>{item.title}</Text>
            </Card>
        );
    }
    const onGenreChange=(index,item)=>{
        addOrRemoveFromGenre(item.id);
        setGenreData(pre => {
            pre[index].check = pre[index].check ===  0 ? 1 : 0;
            return [...pre];
        });
    };

    function GenreItem({ item,index }) {
        return (
            <Card style={{padding: 12,margin:6,flex: 1, flexDirection: 'column',height:150,width:150 ,backgroundColor:item.check===0?'#fff':'#33CCFF'}}
                  onPress={()=>onGenreChange(index,item)}
                  elevation={8}>
                <View style={{flex:1,justifyContent:'center'}}>
                    <View style={{alignItems:'center'}}>
                        <Image source={{uri: item.image}} style={{height: 60, width: 60}}/>
                    </View>
                    <Text style={{textAlign:'center',marginTop:8,color: item.check===0?'#000':'#fff'}}>{item.title}</Text>
                </View>
            </Card>
        );
    }

    return(
        <ScrollView>
            <View style={{backgroundColor:'#fff',padding:12}}>
                <Text style={{fontSize: 14, fontWeight: 'bold'}}>Select your language(s)</Text>
                <FlatList
                    style={{marginTop:8,padding:4}}
                    data={languageData}
                    renderItem={({ item,index}) => <LanguageItem item={item} index={index} />}
                    keyExtractor={item => item.slug}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                />
                <Text style={{marginTop:12,fontSize: 14, fontWeight: 'bold'}}>Select genre(s)</Text>
                <FlatList
                    style={{marginTop:8,padding:4}}
                    data={genreData}
                    renderItem={({ item ,index}) => <GenreItem item={item} index={index}/>}
                    keyExtractor={item => item.slug}
                    numColumns={2}
                />

            </View>
        </ScrollView>
    )
};

export default SelectPreference;
