import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {createLogger} from 'redux-logger';
import appReducer from '../ops/appReducer';
//import authReducer from '../components/Auth/ops/auth.reducer';

const logger = createLogger({
    level: 'warn',
    diff: true,
});


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
let middleware = applyMiddleware(thunkMiddleware);

const store = createStore(
    combineReducers({
        app: appReducer,
    }),
    composeEnhancers(middleware),
);

export default store;
