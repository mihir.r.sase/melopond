import React, {Component} from 'react';
import {Image, Text, View, ScrollView, FlatList, TextInput, TouchableOpacity} from 'react-native';
import Rahman from '../../assets/images/rahman.png';
import {Button, Card, IconButton} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';
import Song from '../Song';
import AsyncStorage from '@react-native-community/async-storage';
import AppConstants from '../../utils/AppConstants';
import useAxios from 'axios-hooks';
import ApiRoutes from '../../utils/ApiRoutes';
import useLoading from '../../utils/useLoading';
import AppRoutes from '../../utils/AppRoutes';
import {useFocusEffect} from '@react-navigation/native';
import WebView from 'react-native-webview';
import {getConfigWithoutViewProps} from 'react-native/Libraries/Utilities/verifyComponentAttributeEquivalence';

const TRACK_DATA = [
    {
        id: '1',
        name: 'Tera Jaisa Yaar Kaha',
        album: 'Yaarana',
        genre: 'Traditional',
        length: '03:06',
    },
    {
        id: '2',
        name: 'Neele Neele Ambar par',
        album: 'Kalaakaar',
        genre: 'Traditional',
        length: '03:20',
    },
    {
        id: '3',
        name: 'Tujhse Naaraz Nahi Zindagi',
        album: 'Masoom',
        genre: 'Romantic',
        length: '04:02',
    },

];

const YOUTUBE_DATA = [
    {
        id: '1',
        link: 'Tera Jaisa Yaar Kaha',
    },
    {
        id: '2',
        link: 'Tera Jaisa Yaar Kaha',
    },
    {
        id: '3',
        link: 'Tera Jaisa Yaar Kaha',
    },

];


const MusicianProfile = (props) => {

    const [{data, loading, error}, execute] = useAxios({url: ApiRoutes.getMusicianProfile, method: 'POST'}, {manual: true});
    const [{data1, loading1, error1}, execute1] = useAxios({url: ApiRoutes.getMusicianProfileUsingSlug, method: 'POST'}, {manual: true});
    const [{data2, loading2, error2}, execute2] = useAxios({url: ApiRoutes.addVideo, method: 'POST'}, {manual: true});
    const [{data3, loading3, error3}, execute3] = useAxios({url: ApiRoutes.removeVideo, method: 'POST'}, {manual: true});
    useLoading(loading);
    const [profile,setProfile]= React.useState('');
    const [music,setMusic]= React.useState('');
    const [video,setVideo]= React.useState('');
    const [state,setState]= React.useState({
       videoLink:'',
       title:''
    });

    const handleChange = (value, name) => {
        setState(prevState => ({...prevState, [name]: value}));
    };


    useFocusEffect(
        React.useCallback(() => {
            // Do something when the screen is focused
            if(loading===false && data === undefined) {
                execute().then((data) => {
                    setProfile(data.data.profile);
                    setMusic(data.data.music);
                    setVideo(data.data.video);
                }).catch(e => console.log(e));
            }

        }, []),
    );
    React.useLayoutEffect(() => {

        props.navigation.setOptions({
            headerLeft: () => (
                <IconButton icon="menu" color={'#000'} size={25} onPress={() => {
                    props.navigation.openDrawer();
                }}/>), headerRight: () => (
                    <IconButton icon="pencil" color={'#000'} size={25} onPress={() => {
                        props.navigation.navigate(AppRoutes.musicianEditProfile, profile);
                    }}/>
                ),
            });

    });

    const addVideo=()=>{
        if(video.length===3){
            handleChange('', 'videoLink');
            return
        }
        if(state.videoLink.includes('youtube.com/watch?v=')) {
            let split = state.videoLink.split('watch?v=');
            let arr = video.concat({url: split[1]});
            handleChange('', 'videoLink');
            setVideo(arr);
            execute2({data:{url:split[1]}}).then(()=>{console.log("Success")}).catch(e=>console.log(e))
        }else{
            console.log("Invalid")
        }
    };
    const removeVideo=(id)=>{

        let temp = video;
        temp.splice(temp.findIndex(function(i){
            return i.id === id;
        }), 1);
        execute3({data:{id:id}}).then(()=>{
            console.log("Success");
            if(temp.length===0){
                setVideo('');
            }else{setVideo(temp)}
        }).catch(e=>console.log(e));

    };

console.log("Render "+JSON.stringify(video));

    function Video({ item}) {

        return (
            <Card style={{ backgroundColor:'#fff',margin:6}}
                  elevation={8}>
                <Icon  style={{marginStart:8}} name="close" size={30} color="#323232" onPress={() => removeVideo(item.id)}/>
                    <WebView
                        style={{ width: 320, height: 230 }}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        source={{ uri: 'https://www.youtube.com/embed/'+item.url }}
                    />

            </Card>
        );
    }
    //"https://www.youtube.com/embed/-ZZPOXn6_9w"

    return (
        <ScrollView>
            <View style={{backgroundColor: '#fff', padding: 18}}>

                <View style={{flexDirection: 'row', flex: 1}}>
                    <View style={{justifyContent: 'center'}}>
                        <Image source={{uri: profile?.profile_pic}}
                               style={{height: 100, width: 100, borderRadius: 100 / 2}}/>
                    </View>
                    <View style={{flex: 1, marginStart: 28}}>
                        <Text style={{fontSize: 20, fontWeight: 'bold'}}>{profile?.name}</Text>
                        <Text style={{fontSize: 14, marginTop: 8}}>{profile?.about_me}</Text>
                    </View>
                </View>



                <View style={{flexDirection: 'row', padding: 8, marginTop: 28, backgroundColor: '#f8f8f8'}}>
                    <View style={{flex: 1, padding: 8}}>
                        <Text style={{
                            textAlign: 'center',
                            color: '#33CCFF',
                            fontSize: 18,
                            fontWeight: 'bold',
                        }}>{profile?.total_like}</Text>
                        <Text style={{textAlign: 'center', fontSize: 14, marginTop: 4}}>Likes</Text>
                    </View>
                    <View style={{
                        flex: 1,
                        padding: 8,
                        borderRightColor: '#323232',
                        borderRightWidth: 1,
                        borderLeftColor: '#323232',
                        borderLeftWidth: 1,
                    }}>
                        <Text style={{
                            textAlign: 'center',
                            color: '#33CCFF',
                            fontSize: 18,
                            fontWeight: 'bold',
                        }}>{profile?.follower}</Text>
                        <Text style={{textAlign: 'center', fontSize: 14, marginTop: 4}}>Followers</Text>
                    </View>
                    <View style={{flex: 1, padding: 8}}>
                        <Text style={{
                            textAlign: 'center',
                            color: '#33CCFF',
                            fontSize: 18,
                            fontWeight: 'bold',
                        }}>₹{profile?.charges}</Text>
                        <Text style={{textAlign: 'center', fontSize: 14, marginTop: 4}}>Charges</Text>
                    </View>
                </View>

               <View style={{padding: 8, marginTop: 28, backgroundColor: '#f8f8f8'}}>
                    <Text style={{fontSize: 14, fontWeight: 'bold'}}>Music Status</Text>
                    <View style={{alignItems: 'center', marginTop: 8}}>
                        <Icon name="cloud-upload" size={30} color="#33CCFF"/>
                    </View>
                    <Text style={{fontSize: 12, textAlign: 'center', marginBottom: 8}}>Select File</Text>
                </View>


                <View style={{marginTop: 28}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Text style={{fontSize: 18, color: '#33CCFF', fontWeight: 'bold'}}>Tracks</Text>
                        <Text style={{fontSize: 14, textAlign: 'right', flex: 1}}
                              onPress={() => {props.navigation.navigate(AppRoutes.songList, {songs: music, title: 'Tracks',});}}
                        >View All</Text>
                    </View>
                    <FlatList
                        style={{marginTop: 8}}
                        data={music}
                        renderItem={({item}) => <Song item={item}/>}
                        keyExtractor={item => item.id}
                    />
                </View>


                <Text style={{fontSize: 18, color: '#33CCFF', fontWeight: 'bold'}}>Youtube Videos</Text>
                    <View style={{flexDirection:'row'}}>
                        <View style={{flex:4,justifyContent:'center'}}>
                            <TextInput onChangeText={text => handleChange(text, 'videoLink')} value={state.videoLink}
                                style={{marginTop: 8, height: 50, backgroundColor: '#f8f8f8', paddingStart: 12}} placeholder={'Video link'}/>
                        </View>
                        <View style={{alignItems:'center',flex:1,justifyContent:'center'}}>
                            <Icon name="plus-circle" size={30} color="#33CCFF" onPress={() => addVideo()}/>
                        </View>
                    </View>



                <FlatList
                    style={{marginTop:8,padding:4}}
                    data={video}
                    renderItem={({ item,index}) => <Video item={item} index={index} />}
                    keyExtractor={item => item.link}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}/>


            </View>
        </ScrollView>
    );
};

export default MusicianProfile;
