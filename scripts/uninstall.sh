#!/bin/bash
PACKAGE_NAME="com.jobdox.timecard"

echo "Clear data"
echo `adb shell pm clear "${PACKAGE_NAME}"`

echo "Uninstalling JobDox Android App:"
echo `adb uninstall "${PACKAGE_NAME}"`
