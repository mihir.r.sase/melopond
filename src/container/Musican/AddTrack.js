import {Text, TextInput, View,ScrollView} from 'react-native';
import React from 'react';
import Select from '../../utils/Dropdown';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Button, Card} from 'react-native-paper';
import useAxios from 'axios-hooks';
import ApiRoutes from '../../utils/ApiRoutes';
import useLoading from '../../utils/useLoading';
import DocumentPicker from 'react-native-document-picker';
import {getFormData} from '../../utils/Webservice';


const AddTrack=(props)=>{

    const [state,setState]= React.useState({
        title:'',
        music_sound:'',
        feeling_id:'',
        language_id:'',
        genre_id:'',
        bpm:120,
        duration:60,
        url:''
    });
    const [prefData,setPrefData]=React.useState({
        feeling_data:[],
        language_data:[],
        genre_data:[],
    });

    const handleChange = (value, name) => {
        setState(prevState => ({...prevState, [name]: value}));
    };


    const [{data, loading, error}, execute] = useAxios({url: ApiRoutes.getLanguageAndGenre, method: 'POST'}, {manual: true});
    const [{data1, loading1, error1}, execute1] = useAxios({url: ApiRoutes.addTrack, method: 'POST'}, {
        manual: true,
        headers: {'Content-Type': 'multipart/form-data'}
    });
    React.useEffect(() => {
        if(loading===false && data===undefined){
            execute().then((data)=>{

                let feeling = data.data.feelings;
                for(let i = 0; i < feeling.length; i++){
                    feeling[i].label = feeling[i]['title'];
                    feeling[i].value = feeling[i]['id'];
                    delete feeling[i].title;
                    delete feeling[i].id;
                }

                let languages = data.data.languages;
                for(let i = 0; i < languages.length; i++){
                    languages[i].label = languages[i]['title'];
                    languages[i].value = languages[i]['id'];
                    delete languages[i].title;
                    delete languages[i].id;
                }
                let genres = data.data.genres;
                for(let i = 0; i < genres.length; i++){
                    genres[i].label = genres[i]['title'];
                    genres[i].value = genres[i]['id'];
                    delete genres[i].title;
                    delete genres[i].id;
                }


                setPrefData({
                    feeling_data:feeling,
                    language_data:languages,
                    genre_data:genres,
                })
            }).catch((e)=>console.log(e));
        }
    });

    const selectFile=()=>{
        DocumentPicker.pick({type: [DocumentPicker.types.audio],readContent:true}).then(response=>{
            console.log(response);
            handleChange({
                name: response.name,
                type: response.type,
                uri: Platform.OS === 'android' ? response.uri : response.uri.replace('file://', '')
            },'url');
        });
    };

    const uploadTrack=()=>{


        execute1({data:getFormData(state)}).then(data=>{
            console.log("Success")
        }).catch(e=>{console.log(e)});
    };



console.log("Render : "+JSON.stringify(prefData));
    return(
        <ScrollView>
        <View style={{flex:1,backgroundColor:'#fff',padding:18}}>
            <Text style={{marginTop: 8}}>Title</Text>
            <TextInput onChangeText={text => handleChange(text, 'title')} value={state.title}
                       style={{
                           marginTop: 4,
                           fontSize: 16,
                           height: 65,
                           backgroundColor: '#f8f8f8',
                           paddingStart: 12,
                       }} placeholder={'Enter title'}/>
            <Text style={{marginTop: 8}}>Music Sound</Text>
            <TextInput onChangeText={text => handleChange(text, 'music_sound')} value={state.music_sound}
                style={{
                    marginTop: 4,
                    fontSize: 16,
                    height: 65,
                    backgroundColor: '#f8f8f8',
                    paddingStart: 12,
                }} placeholder={'Enter music sound'}/>
            <Text style={{marginTop:8}}>Feeling</Text>
            <Select data={prefData?.feeling_data}
                    placeholder='Select Feeling' value={state.feeling_id} onChangeText={(value) => {
                        console.log(value);
                        handleChange(value,'feeling_id');
                    }}
            />
            <Text style={{marginTop:8}}>Language</Text>
            <Select data={prefData?.language_data}
                placeholder='Select Language' value={state.language_id} onChangeText={(value) => {handleChange(value,'language_id');}}
            />
            <Text style={{marginTop:8}}>Genre</Text>
            <Select data={prefData?.genre_data}
                placeholder='Select Genre' value={state.genre_id} onChangeText={(value) => {handleChange(value,'genre_id');}}
            />
            <Text style={{marginTop:8}}>File</Text>
            <View style={{padding: 8, backgroundColor: '#f8f8f8'}}>
                <View style={{alignItems: 'center', marginTop: 8}}>
                    <Icon name="cloud-upload" size={30} color="#33CCFF" onPress={()=>{selectFile()}}/>
                </View>
                <Text style={{fontSize: 12, textAlign: 'center', marginBottom: 8}}>Select File</Text>
            </View>
            <Button style={{height: 50, justifyContent: 'center', marginTop: 26}} mode="contained"
                    onPress={() =>{uploadTrack()}}
                    color={'#33CCFF'} dark={true}>Add</Button>
        </View>
        </ScrollView>
    );
};
export default AddTrack
