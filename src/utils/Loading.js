import React from 'react';
import {Modal, View, Text} from 'react-native';
import {connect} from 'react-redux';
import {ActivityIndicator, Dialog} from 'react-native-paper';

class Loading extends React.Component {
    render() {
        return (
            <Dialog
                visible={this.props.loading}
                // visible={true}
                style={{padding: 28}}
            >
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <ActivityIndicator animating={true} color={'#33CCFF'}/>
                    <Text style={{fontSize: 22, marginStart: 22}}>Loading...</Text>
                </View>
            </Dialog>
        );
    }
}
export default connect(
    state => state.app,
)(Loading);

