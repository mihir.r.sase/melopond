import {Image, Text, TextInput, View, ScrollView, FlatList, TouchableOpacity} from 'react-native';
import React, {Component} from 'react';
import Select from '../../utils/Dropdown';
import {Button, Card, IconButton} from 'react-native-paper';
import Arijit from '../../assets/images/arijit.png';
import Sonu from '../../assets/images/sonu.png';
import Rahman from '../../assets/images/rahman.png';
import useAxios from 'axios-hooks';
import ApiRoutes from '../../utils/ApiRoutes';
import useLoading from '../../utils/useLoading';
import AppRoutes from '../../utils/AppRoutes';


const ARTIST_DATA = [
    {
        id: '1',
        title: 'Arijit Singh',
        background: Arijit,
        charges: '₹8000',
    },
    {
        id: '2',
        title: 'Sonu Nigam',
        background: Sonu,
        charges: '₹10000',
    },
    {
        id: '3',
        title: 'A.R. Rahman',
        background: Rahman,
        charges: '₹4500',
    },
];

const MUSICIAN_TYPE =[
    {label:'Musician',value:0},{label:'Band',value:1}
];



const HireMusicianStep1 = (props) => {

    const [state, setState] = React.useState({
        musician_type: 0,
        category_id:'',
        from_charges:'',
        to_charges:''
    });
    const [musicianData,setMusicianData]= React.useState();
    const [eventCategories,setEventCategories]= React.useState();
    const handleChange = (value, name) => {
        setState(prevState => ({...prevState, [name]: value}));
    };


    const [{data, loading, error}, execute] = useAxios({
        url: ApiRoutes.filterMusicians,
        method: 'POST',
    }, {manual: true});
    useLoading(loading);

    const [{data1, loading1, error1}, execute1] = useAxios({
        url: ApiRoutes.getEventCategories,
        method: 'POST',
    }, {manual: true});


    React.useEffect(() => {
        if(loading===false && data===undefined){
            execute({data:{from_charges:state.from_charges,to_charges:state.to_charges,musician_type:state.musician_type,category_id:state.category_id}}).then((data)=>{
                setMusicianData(data.data);
                    execute1().then((data1)=>{
                        setEventCategories(data1.data);
                    }).catch(e=>console.log(e));

            }).catch((e)=>console.log(e));
        }

    });

    const filter=()=>{

        execute({data:{from_charges:state.from_charges,to_charges:state.to_charges,musician_type:state.musician_type,category_id:state.category_id}}).then((data)=>{
            setMusicianData(data.data);
        }).catch((e)=>console.log(e));
    };

    React.useLayoutEffect(() => {
        props.navigation.setOptions({
            headerLeft: () => (
                <IconButton icon="menu" color={'#000'} size={25} onPress={() => {
                    props.navigation.openDrawer();
                }}/>
            )
        });
    });

    function ArtistItem({item}) {
        return (

            <TouchableOpacity onPress={() =>props.navigation.navigate(AppRoutes.hireMusicianStep2,{slug:item.slug})}>
                <View style={{backgroundColor: '#fff', height: 130, width: 130,margin:4,flex:1,flexDirection:'column',alignItems:'center'}} >
                    <Image source={item.profile_pic===null?Sonu:{uri:item.profile_pic}} style={{height: 130, width: 130, position: 'absolute',borderRadius: 130 / 2}}/>
                    <View style={{flex: 1, justifyContent: 'flex-end'}}>
                        <Text style={{fontSize: 12, textAlign: 'center', color: '#fff',fontWeight:'bold'}}>{item.name}</Text>
                        <Text style={{fontSize: 12, textAlign: 'center', color: '#fff',marginBottom:12,fontWeight:'bold'}}>₹{item.charges}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    return (


            <View style={{backgroundColor: '#fff', padding: 18, flex: 1}}>
                <ScrollView>
                <View style={{flexDirection: 'row'}}>
                    <TextInput onChangeText={text => handleChange(text, 'from_charges')} value={state.from_charges}
                        style={{
                        marginTop: 8,
                        backgroundColor: '#f8f8f8',
                        paddingStart: 12,
                        flex: 1,
                        marginEnd: 4, fontSize: 16, height: 65,
                    }} placeholder={'From Charges(₹)'} keyboardType={'numeric'}/>
                    <TextInput onChangeText={text => handleChange(text, 'to_charges')} value={state.to_charges}
                        style={{
                        marginTop: 8,
                        backgroundColor: '#f8f8f8',
                        paddingStart: 12,
                        flex: 1,
                        marginStart: 4, fontSize: 16, height: 65,
                    }} placeholder={'To Charges(₹)'} keyboardType={'numeric'}/>
                </View>
                <Select data={MUSICIAN_TYPE} placeholder='Musician Type' value={state.musician_type} onChangeText={(value) => {handleChange(value,'musician_type');}} />
                <Select data={eventCategories} placeholder='Event Category' value={state.category_id} onChangeText={(value) => {handleChange(value,'category_id');}} />
                <Button style={{height: 50, justifyContent: 'center', marginTop: 16}} mode="contained"
                        onPress={() => filter()} color={'#33CCFF'} dark={true}>Search</Button>

                <FlatList
                    style={{marginTop: 8, padding: 4}}
                    data={musicianData}
                    renderItem={({item}) => <ArtistItem item={item}/>}
                    keyExtractor={item => item.slug}
                    numColumns={2}
                />
                </ScrollView>
            </View>

    );
};
export default HireMusicianStep1;
