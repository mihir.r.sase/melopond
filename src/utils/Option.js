import {TouchableOpacity} from "react-native";
import {Text} from "react-native-paper";
import React, {Component} from "react";

class Option extends Component {
    onPress = () => {
        const { index, onPress } = this.props;

        if (typeof onPress === "function") {
            onPress(index);
        }
    };

    render() {
        const { selected, title } = this.props;
        return (
            <TouchableOpacity
                style={{
                    backgroundColor: selected ? "lightgray" : "white",
                    padding: 16,
                    width: this.props.width
                }}
                onPress={this.onPress}
            >
                <Text style={{ fontSize: 17}}>{title}</Text>
            </TouchableOpacity>
        );
    }
}

export default Option;