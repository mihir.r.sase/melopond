import {createDrawerNavigator} from '@react-navigation/drawer';
import React, {Component} from 'react';
import Explore from '../container/Explore';
import AppRoutes from './AppRoutes';
import SelectPreference from '../container/SelectPreference';
import {CommonActions, NavigationContainer, useFocusEffect} from '@react-navigation/native';
import MusicianProfile from '../container/Musican/MusicianProfile';
import AsyncStorage from '@react-native-community/async-storage';
import AppConstants from './AppConstants';
import ListenerProfile from '../container/Listener/ListenerProfile';
import {createStackNavigator} from '@react-navigation/stack';
import ListenerEditProfile from '../container/Listener/ListenerEditProfile';
import SongList from '../container/SongList';
import HireMusicianStep1 from '../container/Listener/HireMusicianStep1';
import HireMusicianStep2 from '../container/Listener/HireMusicianStep2';
import HireMusicianRequests from '../container/Listener/HireMusicianRequests';
import MessageList from '../container/MessageList';
import Subscribe from '../container/Subscribe';
import {Button, Modal, Portal, Text} from 'react-native-paper';
import {TextInput, TouchableOpacity, View} from 'react-native';
import SubscribeHistory from '../container/SubscribeHistory';
import MusicianEditProfile from '../container/Musican/MusicianEditProfile';
import HireRequests from '../container/Musican/HireRequests';
import HireCharges from '../container/Musican/HireCharges';
import ChangePassword from '../container/ChangePassword';
import Tracks from '../container/Musican/Tracks';
import AddTrack from '../container/Musican/AddTrack';
import MusicianView from '../container/MusicianView';
import LanguageAndGenres from '../container/LanguageAndGenres';
import Player from '../container/Player';

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

function ListenerProfileNav() {
    return (
        <Stack.Navigator>
            <Stack.Screen name={AppRoutes.listenerProfile} component={ListenerProfile}
                          options={{headerTitle: 'Profile'}}/>
            <Stack.Screen name={AppRoutes.listenerEditProfile} component={ListenerEditProfile}
                          options={{headerTitle: 'Edit Profile'}}/>
            <Stack.Screen name={AppRoutes.songList} component={SongList}/>
            <Stack.Screen name={AppRoutes.musicianProfile} component={MusicianView}  options={{headerTitle: 'Profile'}}/>
        </Stack.Navigator>
    );
}

function ExploreNav() {
    return (
        <Stack.Navigator>
            <Stack.Screen name={AppRoutes.explore} component={Explore} options={{headerTitle: 'Explore'}}/>
            <Stack.Screen name={AppRoutes.songList} component={SongList}/>
            <Stack.Screen name={AppRoutes.musicianProfile} component={MusicianView}  options={{headerTitle: 'Profile'}}/>
            {/*<Stack.Screen name={AppRoutes.player} component={Player} options={{ headerShown: false}}/>*/}
        </Stack.Navigator>
    );
}

function HireMusicianNav() {
    return (
        <Stack.Navigator>
            <Stack.Screen name={AppRoutes.hireMusicianStep1} component={HireMusicianStep1}
                          options={{headerTitle: 'Hire Musician'}}/>
            <Stack.Screen name={AppRoutes.hireMusicianStep2} component={HireMusicianStep2}
                          options={{headerTitle: 'Hire Musician'}}/>
        </Stack.Navigator>
    );
}

function HireMusicianRequestNav() {
    return (
        <Stack.Navigator>
            <Stack.Screen name={AppRoutes.hireMusicianList} component={HireMusicianRequests}
                          options={{headerTitle: 'Hire Requests'}}/>
        </Stack.Navigator>
    );
}

function MessagesNav() {
    return (
        <Stack.Navigator>
            <Stack.Screen name={AppRoutes.messages} component={MessageList}
                          options={{headerTitle: 'Messages'}}/>
        </Stack.Navigator>
    );
}

function SubscribeNav() {
    return (
        <Stack.Navigator>
            <Stack.Screen name={AppRoutes.subscribe} component={Subscribe}
                          options={{headerTitle: 'Subscribe'}}/>
            <Stack.Screen name={AppRoutes.subscribeHistory} component={SubscribeHistory}
                          options={{headerTitle: 'Subscribe History'}}/>
        </Stack.Navigator>
    );
}

function MusicianProfileNav() {
    return (
        <Stack.Navigator>
            <Stack.Screen name={AppRoutes.musicianProfile} component={MusicianProfile}
                          options={{headerTitle: 'Profile'}}/>
            <Stack.Screen name={AppRoutes.musicianEditProfile} component={MusicianEditProfile}
                          options={{headerTitle: 'Edit Profile'}}/>
            <Stack.Screen name={AppRoutes.songList} component={SongList}/>
        </Stack.Navigator>
    );
}

function HireRequestNav() {
    return (
        <Stack.Navigator>
            <Stack.Screen name={AppRoutes.hireMusicianList} component={HireRequests}
                          options={{headerTitle: 'Hire Requests'}}/>
        </Stack.Navigator>
    );
}

function HireChargesNav() {
    return (
        <Stack.Navigator>
            <Stack.Screen name={AppRoutes.hireCharges} component={HireCharges}
                          options={{headerTitle: 'Hire Charges'}}/>
        </Stack.Navigator>
    );
}
function PreferenceNav() {
    return (
        <Stack.Navigator>
            <Stack.Screen name={AppRoutes.preferences} component={LanguageAndGenres}
                          options={{headerTitle: 'Preferences'}}/>
        </Stack.Navigator>
    );
}


function TracksNav() {
    return (
        <Stack.Navigator>
            <Stack.Screen name={AppRoutes.tracks} component={Tracks}
                          options={{headerTitle: 'Tracks'}}/>
            <Stack.Screen name={AppRoutes.addTrack} component={AddTrack}
                          options={{headerTitle: 'Add Track'}}/>

        </Stack.Navigator>
    );
}

function ChangePasswordNav() {
    return (
        <Stack.Navigator>
            <Stack.Screen name={AppRoutes.changePassword} component={ChangePassword}
                          options={{headerTitle: 'Change Password'}}/>
        </Stack.Navigator>
    );
}

function Logout(props){


    const [visible,setVisible] = React.useState(true);
    useFocusEffect(
        React.useCallback(() => {
            // Do something when the screen is focused
            setVisible(true);
            return () => {
                // Do something when the screen is unfocused
                // Useful for cleanup functions
            };
        }, []),
    );
    const logout=()=>{
        AsyncStorage.clear().then(()=>{
            setVisible(false);
            props.navigation.dispatch(
                CommonActions.reset({
                    index: 1,
                    routes: [
                        { name: AppRoutes.login }
                    ],
                })
            );
        }).catch(e=>console.log(e));
    };

    return(<View>
        <Portal>
            <Modal visible={visible} >
                <View style={{backgroundColor:'#fff',paddingTop:22,paddingStart:22,paddingEnd:22,paddingBottom:12,marginStart:30,marginEnd:30}}>
                    <Text style={{fontSize:16}}>Are you sure you want to logout?</Text>
                    <View style={{flexDirection:'row',justifyContent:'flex-end',marginTop:14}}>
                        <TouchableOpacity onPress={() => {setVisible(false);props.navigation.navigate(AppRoutes.explore)}}>
                            <Text style={{fontWeight:'bold',fontSize:16,marginEnd:18}}>No</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => logout()}>
                            <Text style={{fontWeight:'bold',fontSize:16}}>Yes</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>

        </Portal>
    </View>)
}

function MyDrawer(props) {
    console.log(props.route.params.role);
    let role = props.route.params.role;
    if (role === 'listener') {
        return (
            <Drawer.Navigator
                drawerContentOptions={{activeTintColor: '#33CCFF', itemStyle: {justifyContent: 'center'}}}>
                <Drawer.Screen name={AppRoutes.explore} component={ExploreNav} options={{drawerLabel: 'Explore  '}}/>
                <Drawer.Screen name={AppRoutes.preferences} component={PreferenceNav}
                               options={{drawerLabel: 'Preferences '}}/>
                <Drawer.Screen name={AppRoutes.listenerProfile} component={ListenerProfileNav}
                               options={{drawerLabel: 'Profile '}}/>
                <Drawer.Screen name={AppRoutes.hireMusicianStep1} component={HireMusicianNav}
                               options={{drawerLabel: 'Hire Musician    '}}/>
                <Drawer.Screen name={AppRoutes.hireMusicianList} component={HireMusicianRequestNav}
                               options={{drawerLabel: 'Hire Requests   '}}/>
                <Drawer.Screen name={AppRoutes.messages} component={MessagesNav}
                               options={{drawerLabel: 'Messages   '}}/>
                <Drawer.Screen name={AppRoutes.subscribe} component={SubscribeNav}
                               options={{drawerLabel: 'Subscribe   '}}/>
                <Drawer.Screen name={AppRoutes.changePassword} component={ChangePasswordNav}
                               options={{drawerLabel: 'Change Password   '}}/>
                <Drawer.Screen name={AppRoutes.logout} component={Logout}
                               options={{drawerLabel: 'Logout   '}}/>
            </Drawer.Navigator>)
            ;

    } else {
        return (
            <Drawer.Navigator
                drawerContentOptions={{activeTintColor: '#33CCFF', itemStyle: {justifyContent: 'center'}}}>
                <Drawer.Screen name={AppRoutes.explore} component={ExploreNav} options={{drawerLabel: 'Explore  '}}/>
                <Drawer.Screen name={AppRoutes.preferences} component={PreferenceNav}
                               options={{drawerLabel: 'Preferences '}}/>
                <Drawer.Screen name={AppRoutes.musicianProfile} component={MusicianProfileNav}
                               options={{drawerLabel: 'Profile '}}/>
                <Drawer.Screen name={AppRoutes.tracks} component={TracksNav}
                               options={{drawerLabel: 'Tracks   '}}/>
                <Drawer.Screen name={AppRoutes.hireMusicianList} component={HireRequestNav}
                               options={{drawerLabel: 'Hire Requests   '}}/>
                <Drawer.Screen name={AppRoutes.hireCharges} component={HireChargesNav}
                               options={{drawerLabel: 'Hire Charges   '}}/>
                <Drawer.Screen name={AppRoutes.messages} component={MessagesNav}
                               options={{drawerLabel: 'Messages   '}}/>
                <Drawer.Screen name={AppRoutes.changePassword} component={ChangePasswordNav}
                               options={{drawerLabel: 'Change Password   '}}/>
                <Drawer.Screen name={AppRoutes.logout} component={Logout}
                               options={{drawerLabel: 'Logout   '}}/>
            </Drawer.Navigator>
        );


    }
}

export default MyDrawer;
