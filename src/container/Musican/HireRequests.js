import React from 'react';
import useAxios from 'axios-hooks';
import ApiRoutes from '../../utils/ApiRoutes';
import useLoading from '../../utils/useLoading';
import {useFocusEffect} from '@react-navigation/native';
import {Button, Card, Chip, IconButton, Modal, Portal, Text} from 'react-native-paper';
import {FlatList, Linking, TextInput, TouchableOpacity, View} from 'react-native';
import AppConstants from '../../utils/AppConstants';
import AppRoutes from '../../utils/AppRoutes';

const HireRequests =(props)=>{

    const [requestList,setRequestList]= React.useState();
    const [modal,setModal]= React.useState({
        visible: false,
        data: '',
        type:'',
        index:''
    });
    const handleChange = (value, name) => {
        setModal(prevState => ({...prevState, [name]: value}));
    };

    const [{data, loading, error}, execute] = useAxios({
        url: ApiRoutes.hireRequestsMusician,
        method: 'POST',
    }, {manual: true});
    useLoading(loading);

    const [{data1, loading1, error1}, execute1] = useAxios({
        url: ApiRoutes.changeRequestStatus,
        method: 'POST',
    }, {manual: true});


    useFocusEffect(
        React.useCallback(() => {
            // Do something when the screen is focused
            if(loading===false && data===undefined){
                execute().then((data)=>{
                    setRequestList(data.data.list);
                }).catch(e=>console.log(e));
            }
            return () => {
                // Do something when the screen is unfocused
                // Useful for cleanup functions
            };
        }, []),
    );

    React.useLayoutEffect(() => {
        props.navigation.setOptions({
            headerLeft: () => (
                <IconButton icon="menu" color={'#000'} size={25} onPress={() =>{props.navigation.openDrawer();} }/>
            ),
        });
    });


    const Confirmation=()=>{

        const performOp=()=>{
            _hideModal();
            execute1({data:{id:modal.data.id,status: modal.type==='Accept'?1:2}}).then(()=>{
                setRequestList(s => {
                    s[modal.index].status = modal.type;
                    return [...s];
                });


            }).catch(e=>{console.log(e)})



        };

        const _hideModal = () => handleChange(false,'visible');
        return(
            <View>
            <Portal>
                <Modal visible={modal.visible} onDismiss={_hideModal}>
                    <View style={{backgroundColor:'#fff',paddingTop:22,paddingStart:22,paddingEnd:22,paddingBottom:12,marginStart:30,marginEnd:30}}>
                        <Text style={{fontSize:16}}>Are you sure you want to {modal.type} the request?</Text>
                        <View style={{flexDirection:'row',justifyContent:'flex-end',marginTop:14}}>
                            <TouchableOpacity onPress={() => {_hideModal()}}>
                                <Text style={{fontWeight:'bold',fontSize:16,marginEnd:18}}>No</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => performOp()}>
                                <Text style={{fontWeight:'bold',fontSize:16}}>Yes</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>

            </Portal>
        </View>
        )
    };

    function RequestItem({item,index}) {
        return (

            <Card style={{backgroundColor: '#fff',margin:8,padding:12}} elevation={8}>
                <View style={{flex:1}}>
                    <View style={{flexDirection:'row',marginBottom:4}}>
                        <Text style={{flex:1,fontWeight:'bold'}}>Event</Text>
                        <Text style={{flex:1,textAlign:'right'}}>{item.event_category}</Text>
                    </View>
                    <View style={{flexDirection:'row',marginBottom:4}}>
                        <Text style={{flex:1,fontWeight:'bold'}}>Event Date</Text>
                        <Text style={{flex:1,textAlign:'right'}}>{item.event_date}</Text>
                    </View>
                    <View style={{flexDirection:'row',marginBottom:4}}>
                        <Text style={{flex:1,fontWeight:'bold'}}>Name</Text>
                        <Text style={{flex:1,textAlign:'right'}}>{item.name}</Text>
                    </View>
                    <View style={{flexDirection:'row',marginBottom:4}}>
                        <Text style={{flex:1,fontWeight:'bold'}}>Budget</Text>
                        <Text style={{flex:1,textAlign:'right'}}>₹{item.budget}</Text>
                    </View>
                    <View style={{flexDirection:'row',marginBottom:4}}>
                        <Text style={{flex:1,fontWeight:'bold'}}>Location</Text>
                        <Text style={{flex:1,textAlign:'right'}}>{item.location}</Text>
                    </View>
                    <View style={{flexDirection:'row',marginBottom:4}}>
                        <Text style={{flex:1,fontWeight:'bold'}}>Mobile</Text>
                        <Text style={{flex:1,textAlign:'right'}}>{item.mobile_no}</Text>
                    </View>
                    <View style={{flexDirection:'row',marginBottom:4}}>
                        <Text style={{flex:1,fontWeight:'bold'}}>Status</Text>
                        <View style={{justifyContent:'flex-end'}}>
                            <Chip textStyle={{color:'#fff'}}
                                  style={item.status==='Received'?{backgroundColor:'#ffc107'}:(item.status==='Accept'?{backgroundColor:'#4caf50'}:{backgroundColor:'#ff5722'})}>{item.status}</Chip>
                        </View>
                        {/*<Text style={{flex:1,textAlign:'right'}}>{item.status}</Text>*/}
                    </View>
                    <View style={{flexDirection:'row',marginBottom:8}}>
                        <Text style={{flex:1,fontWeight:'bold'}}>Message</Text>
                        <Text style={{flex:1,textAlign:'right'}}>{item.message}</Text>
                    </View>

                    {item.status==='Received' && !item.expire &&
                    <View style={{marginTop:8,flexDirection:'row'}}>
                        <Button style={{height: 50, justifyContent: 'center', flex: 1, marginEnd: 8}} mode="contained"
                                onPress={() => {setModal({visible:true,data:item,type:'Decline',index:index});}} color={'#ff5722'} dark={true}>Decline</Button>
                        <Button style={{height: 50, justifyContent: 'center', flex: 1, marginStart: 8}} mode="contained"
                                onPress={() => {setModal({visible:true,data:item,type:'Accept',index:index});}} color={'#4caf50'} dark={true}>Accept</Button>
                    </View>
                    }

                </View>
            </Card>
        );
    }

    return(
        <View style={{backgroundColor: '#fff', padding: 10,flex:1}}>
            <Confirmation/>
            <FlatList
                style={{marginTop: 8}}
                data={requestList}
                renderItem={({item,index}) => <RequestItem item={item} index={index}/>}
                keyExtractor={item => item.id.toString()}
            />

        </View>
    )
};

export default HireRequests;
